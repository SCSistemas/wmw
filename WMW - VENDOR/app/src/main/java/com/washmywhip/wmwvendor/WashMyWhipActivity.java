package com.washmywhip.wmwvendor;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.washmywhip.wmwvendor.adapter.NavDrawerListAdapter;
import com.washmywhip.wmwvendor.adapter.RecyclerItemClickListener;
import com.washmywhip.wmwvendor.adapter.ScheduleAdapter;
import com.washmywhip.wmwvendor.classes.NavDrawerItem;
import com.washmywhip.wmwvendor.classes.Schedule;
import com.washmywhip.wmwvendor.services.WMWVendorEngine;
import com.washmywhip.wmwvendor.util.RoundedImageView;
import com.washmywhip.wmwvendor.util.ScrimInsetsFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by enny.querales on 13/6/2016.
 */
public class WashMyWhipActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ListView ndList;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ScrimInsetsFrameLayout sifl;
    private SharedPreferences mSharedPreferences;
    private View mProgressView;
    RecyclerView mView;
    ArrayList<Schedule> mSchedules;
    private GridLayoutManager mLayoutManager;
    private ScheduleAdapter mScheduleAdapter;
    private TextView mEmptyView;

    private RoundedImageView mImageProfile;
    private TextView mTextNameProfile;
    private LinearLayout mLinearProfile;

    private Activity mActivity;

    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private WMWVendorEngine mWMWVendorEngine;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wash_my_wip);
        mActivity = this;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mProgressView = findViewById(R.id.progress_bar);
        mWMWVendorEngine = new WMWVendorEngine();
        sifl = (ScrimInsetsFrameLayout) findViewById(R.id.scrimInsetsFrameLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mEmptyView = (TextView) findViewById(R.id.schedule_empty_view);
        ndList = (ListView) findViewById(R.id.mListView);
        mView = (RecyclerView) findViewById(R.id.scheduleGridView);
        mLayoutManager = new GridLayoutManager(WashMyWhipActivity.this, 1);
        mView.setLayoutManager(mLayoutManager);
        mScheduleAdapter = new ScheduleAdapter(WashMyWhipActivity.this, new ArrayList<Schedule>());
        mView.setAdapter(mScheduleAdapter);
        mView.addOnItemTouchListener(new RecyclerItemClickListener(WashMyWhipActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        Intent i = new Intent(WashMyWhipActivity.this, ListDetailScheduleActivity.class);
                        final Schedule selected = mScheduleAdapter.getSchedule(position);
                        i.putExtra("schedule", selected);
                        startActivity(i);
                    }
                })
        );
        mImageProfile = (RoundedImageView) findViewById(R.id.profilePicture);
        mTextNameProfile = (TextView) findViewById(R.id.nameProfile);
        mLinearProfile = (LinearLayout) findViewById(R.id.linearProfile);
        mLinearProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WashMyWhipActivity.this, ProfileActivity.class));
                drawerLayout.closeDrawer(sifl);
            }
        });

        String name = mSharedPreferences.getString("Username", "");
        int vendorID = Integer.parseInt(mSharedPreferences.getString("VendorID", "-1"));
        if (vendorID >= 0) {
            Picasso.with(mActivity)
                    .load(mActivity.getResources().getString(R.string.url_app) + "/VendorAvatarImages/vendor" + vendorID + "avatar.jpg")
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .resize(200, 200)
                    .centerCrop().placeholder(R.drawable.picture).error(R.drawable.picture).into(mImageProfile);
        }
        if (name.isEmpty()) {
            name = "WMW";
        }
        mTextNameProfile.setTypeface(mFont);
        mTextNameProfile.setText(name.toUpperCase());
        TextView seeMyProfile = (TextView) findViewById(R.id.see_my_profile);
        seeMyProfile.setTypeface(mFont);
        ArrayList<NavDrawerItem> data = new ArrayList<>();
        data.add(new NavDrawerItem("ABOUT", R.drawable.abouticon));

        ndList.setAdapter(new NavDrawerListAdapter(this, R.layout.nav_drawer_item, data));

        ndList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (pos == 0) {
                    startActivity(new Intent(WashMyWhipActivity.this, AboutActivity.class));
                }

                ndList.setItemChecked(pos, true);
                drawerLayout.closeDrawer(sifl);
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.hamburger, R.string.drawerOpen, R.string.drawerClose) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.hamburger);

        showAlertDialog();
        getSchedules();
    }

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_loupper, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();
        if (id == R.id.home) {
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    public void showAlertDialog() {
        dialogBuilder = new AlertDialog.Builder(WashMyWhipActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_welcome, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        TextView mSuccess = (TextView) dialogView.findViewById(R.id.welcome_success);
        mSuccess.setTypeface(mFont);
        TextView mWelcome = (TextView) dialogView.findViewById(R.id.welcome_welcome);
        mWelcome.setTypeface(mFontAvenir);
        TextView mEmail = (TextView) dialogView.findViewById(R.id.welcome_name);
        String email = mSharedPreferences.getString("email", "");
        mEmail.setTypeface(mFontAvenir);
        mEmail.setText(email);
        TextView mThanks = (TextView) dialogView.findViewById(R.id.welcome_thanks);
        mThanks.setTypeface(mFontAvenir);
        LinearLayout linearWelcome = (LinearLayout) dialogView.findViewById(R.id.linear_dialog_welcome);
        linearWelcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }


    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void getSchedules() {
        showProgress(true);
        int vendorID = Integer.parseInt(mSharedPreferences.getString("VendorID", "-1"));
        final ArrayList<Schedule> theSchedules = new ArrayList<>();
        if (vendorID >= 0) {
            showProgress(true);
            mWMWVendorEngine.getScheduledWashes(vendorID, new Callback<List<JSONObject>>() {
                @Override
                public void success(List<JSONObject> jsonObject, Response response) {
                    String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                    JSONArray mArray = null;
                    try {
                        mArray = new JSONArray(responseString);
                        for (int i = 0; i < mArray.length(); i++) {
                            JSONObject schedule = mArray.getJSONObject(i);
                            Schedule newSchedule = new Schedule(schedule);
                            theSchedules.add(newSchedule);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    showProgress(false);
                    mSchedules = theSchedules;
                    if (mSchedules != null) {
                        if (mSchedules.size() > 0) {
                            for (int i = 0; i < mSchedules.size(); i++) {
                                mScheduleAdapter.add(mSchedules.get(i));
                            }
                        } else {
                            mEmptyView.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mEmptyView.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("getSchedules", "failz: " + error.toString());
                    mEmptyView.setVisibility(View.VISIBLE);
                    showProgress(false);
                }
            });
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }
}


