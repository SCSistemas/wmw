package com.washmywhip.wmw;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {
    Button login;
    Button signup;
    Typeface mFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        login = (Button) findViewById(R.id.loginButton);
        login.setOnClickListener(this);
        signup = (Button) findViewById(R.id.signUpButton);
        login.setTypeface(mFont);
        signup.setOnClickListener(this);
        signup.setTypeface(mFont);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.loginButton) {
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
            //finish();
        }
        if (v.getId() == R.id.signUpButton) {
            Intent i = new Intent(this, SignUpActivity.class);
            startActivity(i);
            //finish();
        }
    }
}
