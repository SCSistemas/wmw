package com.washmywhip.wmw;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.washmywhip.wmw.adapters.CardAdapter;
import com.washmywhip.wmw.adapters.RecyclerItemClickListener;
import com.washmywhip.wmw.classes.Card;
import com.washmywhip.wmw.services.WashMyWhipEngine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private WashMyWhipEngine mWashMyWhipEngine;
    private SharedPreferences mSharedPreferences;
    RecyclerView mView;
    Button addPaymentButton;
    String[] list;
    private int userID;
    Map<String, Object> paymentData;
    ArrayList<Card> mCards;
    String defaultCard;
    private GridLayoutManager mLayoutManager;
    private CardAdapter mCardAdapter;
    private View mProgressView;
    ImageView onBack;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private TextView mTitlePayment;
    private TextView mDescPayment;
    private LinearLayout mLinearHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(PaymentActivity.this);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        mView = (RecyclerView) findViewById(R.id.cardGridView);
        addPaymentButton = (Button) findViewById(R.id.addPaymentButton);
        mProgressView = findViewById(R.id.progress_bar);
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        addPaymentButton.setTypeface(mFont);
        mTitlePayment = (TextView) findViewById(R.id.title_payment);
        mTitlePayment.setTypeface(mFont);
        mDescPayment = (TextView) findViewById(R.id.desc_payment);
        mDescPayment.setTypeface(mFontAvenir);
        mLinearHeader = (LinearLayout) findViewById(R.id.linear_payment_header);
        defaultCard = null;
        mWashMyWhipEngine = new WashMyWhipEngine();
        mCards = new ArrayList<>();
        userID = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        mLayoutManager = new GridLayoutManager(PaymentActivity.this, 1);
        mView.setLayoutManager(mLayoutManager);
        mCardAdapter = new CardAdapter(PaymentActivity.this, new ArrayList<Card>());
        mView.setAdapter(mCardAdapter);
        mView.addOnItemTouchListener(new RecyclerItemClickListener(PaymentActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this);
                        builder.setTitle(getResources().getString(R.string.hold_on));
                        builder.setMessage(getResources().getString(R.string.delete_default_schedule));
                        builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final AlertDialog.Builder confirmBuilder = new AlertDialog.Builder(PaymentActivity.this);
                                confirmBuilder.setTitle(getResources().getString(R.string.hold_on));
                                confirmBuilder.setMessage(getResources().getString(R.string.sure_delete_card));
                                confirmBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Card pendingDeleteCard = mCardAdapter.getCard(position);
                                        mCardAdapter.remove(position);
                                        mWashMyWhipEngine.deleteStripeCard(userID, pendingDeleteCard.getId(), new Callback<Object>() {
                                            @Override
                                            public void success(Object s, Response response) {
                                                Log.d("Delete Card", "success: " + s.toString());
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                Toast.makeText(PaymentActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                        dialog.cancel();
                                    }
                                });
                                confirmBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                                confirmBuilder.show();
                                dialog.cancel();
                            }
                        });
                        builder.setPositiveButton("Default", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final AlertDialog.Builder confirmBuilder = new AlertDialog.Builder(PaymentActivity.this);
                                confirmBuilder.setTitle(getResources().getString(R.string.hold_on));
                                confirmBuilder.setMessage(getResources().getString(R.string.sure_default_card));
                                confirmBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Card newDefaultCard = mCardAdapter.getCard(position);
                                        mWashMyWhipEngine.changeDefaultStripeCard(userID, newDefaultCard.getId(), new Callback<Object>() {
                                            @Override
                                            public void success(Object s, Response response) {
                                                while (mCardAdapter.getItemCount() > 0) {
                                                    mCardAdapter.remove(0);
                                                }
                                                getCards();
                                            }

                                            @Override
                                            public void failure(RetrofitError error) {
                                                Toast.makeText(PaymentActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                        dialog.cancel();
                                    }
                                });
                                confirmBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                                confirmBuilder.show();
                                dialog.cancel();
                            }
                        });
                        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        builder.show();
                    }
                })
        );
        getCards();
        addPaymentButton.setOnClickListener(this);
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void getCards() {
        userID = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        final ArrayList<Card> theCards = new ArrayList<>();
        if (userID >= 0) {
            showProgress(true);
            mWashMyWhipEngine.getStripeCustomer(userID, new Callback<JSONObject>() {
                @Override
                public void success(JSONObject o, Response response) {
                    String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                    JSONObject json = null;
                    try {
                        json = new JSONObject(responseString);
                        paymentData = jsonToMap(json);
                        defaultCard = json.getString("default_source");
                        mSharedPreferences.edit().putString("default_source", defaultCard).apply();
                        String data = json.getString("sources");
                        JSONObject cards = new JSONObject(data);
                        Log.d("getCards", json.toString());
                        if (cards.get("object").equals("list")) {
                            //multiple cards
                            String cardString = cards.getString("data");
                            JSONArray jsonArray = new JSONArray(cardString);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject card = jsonArray.getJSONObject(i);
                                String expMonth = card.getString("exp_month");
                                String expYear = card.getString("exp_year");
                                String lastFour = card.getString("last4");
                                String cardType = card.getString("brand");
                                String exYearShort = expYear.substring(expYear.length() - 2);
                                String expirationDate = expMonth + "/" + exYearShort;
                                String id = card.getString("id");
                                Card newCard;
                                if (id.equals(defaultCard)) {
                                    newCard = new Card(id, cardType, lastFour, expirationDate, true);
                                } else {
                                    newCard = new Card(id, cardType, lastFour, expirationDate, false);
                                }
                                theCards.add(newCard);
                            }
                            mCards = theCards;
                            int numCards = theCards.size();
                            list = new String[numCards];
                            for (int j = 0; j < numCards; j++) {
                                list[j] = theCards.get(j).toString();
                                mCardAdapter.add(theCards.get(j));
                            }
                        } else {
                            //just one card
                            String card = cards.getString("data");
                            JSONObject theCard = new JSONObject(card);
                            String expMonth = theCard.getString("exp_month");
                            String expYear = theCard.getString("exp_year");
                            String exYearShort = expYear.substring(expYear.length() - 2);
                            String expirationDate = expMonth + "/" + exYearShort;
                            String lastFour = theCard.getString("last4");
                            String cardType = theCard.getString("brand");
                            String id = theCard.getString("id");
                            Card newCard;
                            if (id.equals(defaultCard)) {
                                newCard = new Card(id, cardType, lastFour, expirationDate, true);
                            } else {
                                newCard = new Card(id, cardType, lastFour, expirationDate, false);
                            }
                            theCards.add(newCard);
                            mCards = theCards;
                            list = new String[1];
                            list[0] = newCard.toString();
                            mCardAdapter.add(newCard);
                        }
                        addPaymentButton.setVisibility(View.VISIBLE);
                        showProgress(false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("getCards", "failz: " + error.toString());
                    showProgress(false);
                    mLinearHeader.setVisibility(View.GONE);
                    showAlertDialog();
                }
            });
        }
    }

    public void showAlertDialog() {
        android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(PaymentActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_no_card, null);
        dialogBuilder.setView(dialogView);
        final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        LinearLayout linearWelcome = (LinearLayout) dialogView.findViewById(R.id.linear_dialog_no_card);
        Button yes = (Button) linearWelcome.findViewById(R.id.dialog_no_card_yes);
        Button later = (Button) linearWelcome.findViewById(R.id.dialog_no_card_later);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PaymentActivity.this, AddPaymentActivity.class);
                startActivityForResult(i, 7);
                finish();
            }
        });
        later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == addPaymentButton.getId()) {
            Intent i = new Intent(PaymentActivity.this, AddPaymentActivity.class);
            startActivityForResult(i, 7);
            finish();
        }
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();
        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == 7) {
                if (resultCode == Activity.RESULT_OK) {
                    getCards();
                }
            }
        }
    }
}
