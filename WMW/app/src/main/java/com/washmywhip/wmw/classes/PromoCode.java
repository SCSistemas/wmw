package com.washmywhip.wmw.classes;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class PromoCode {

    private String promoCodeID;
    private String sponsorName;
    private String code;
    private String discount;
    private String discountDescription;
    private String maxUses;
    private String redemptions;
    private String remainingUses;
/*    {"PromoCodeID":"2","SponsorName":"Uber Miami","Code":"DOL5",
            "Expired":"0","Discount":"$5","DiscountDescription":"$5.00 off your next wash.",
            "MaxUses":"1","DisplayPopup":"0","PopupText":null,"HasPopupImage":"0","PopupImage":null,
            "CreationDate":"2016-07-04 14:27:34","Redemptions":"0","RemainingUses":"4"}*/

    public PromoCode() {
    }




    public PromoCode(JSONObject jsonData) {
        Map<String, Object> promoCodeMap = new HashMap<>();
        try {
            promoCodeMap = jsonToMap(jsonData);
        } catch (JSONException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }

        Object[] keyList = promoCodeMap.keySet().toArray();
        for (Object key : keyList) {
            Log.d("making promoCodeMap", "key: " + key.toString() + "  value: " + promoCodeMap.get(key).toString());
        }

        this.setPromoCodeID(promoCodeMap.get("PromoCodeID").toString());
        this.setSponsorName(promoCodeMap.get("SponsorName").toString());
        this.setCode(promoCodeMap.get("Code").toString());
        this.setDiscount(promoCodeMap.get("Discount").toString());
        this.setDiscountDescription(promoCodeMap.get("DiscountDescription").toString());
        this.setMaxUses(promoCodeMap.get("MaxUses").toString());
        this.setRedemptions(promoCodeMap.get("Redemptions").toString());
        this.setRemainingUses(promoCodeMap.get("RemainingUses").toString());
    }



    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public String getPromoCodeID() {
        return promoCodeID;
    }

    public void setPromoCodeID(String promoCodeID) {
        this.promoCodeID = promoCodeID;
    }

    public String getSponsorName() {
        return sponsorName;
    }

    public void setSponsorName(String sponsorName) {
        this.sponsorName = sponsorName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountDescription() {
        return discountDescription;
    }

    public void setDiscountDescription(String discountDescription) {
        this.discountDescription = discountDescription;
    }

    public String getMaxUses() {
        return maxUses;
    }

    public void setMaxUses(String maxUses) {
        this.maxUses = maxUses;
    }

    public String getRedemptions() {
        return redemptions;
    }

    public void setRedemptions(String redemptions) {
        this.redemptions = redemptions;
    }

    public String getRemainingUses() {
        return remainingUses;
    }

    public void setRemainingUses(String remainingUses) {
        this.remainingUses = remainingUses;
    }
}
