package com.washmywhip.wmw;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.washmywhip.wmw.adapters.RecyclerItemClickListener;
import com.washmywhip.wmw.adapters.ScheduleAdapter;
import com.washmywhip.wmw.classes.Car;
import com.washmywhip.wmw.classes.Schedule;
import com.washmywhip.wmw.services.WashMyWhipEngine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by enny.querales on 12/7/2016.
 */
public class ListScheduleActivity extends AppCompatActivity {

    private WashMyWhipEngine mWashMyWhipEngine;
    private SharedPreferences mSharedPreferences;
    RecyclerView mView;
    private int userID;
    ArrayList<Schedule> mSchedules;
    private GridLayoutManager mLayoutManager;
    private ScheduleAdapter mScheduleAdapter;
    private TextView mTitleSchedule;
    private TextView mDescSchedule;
    private View mProgressView;
    ImageView onBack;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private ArrayList<Car> mCars;
    private TextView mEmptyView;
    private LinearLayout mLinearHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_schedule);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ListScheduleActivity.this);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        mView = (RecyclerView) findViewById(R.id.scheduleGridView);
        mProgressView = findViewById(R.id.progress_bar);
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mEmptyView = (TextView) findViewById(R.id.schedule_empty_view);
        mLinearHeader = (LinearLayout) findViewById(R.id.linear_schedule_header);
        mTitleSchedule = (TextView) findViewById(R.id.title_schedule);
        mTitleSchedule.setTypeface(mFont);
        mDescSchedule = (TextView) findViewById(R.id.desc_schedule);
        mDescSchedule.setTypeface(mFontAvenir);
        mWashMyWhipEngine = new WashMyWhipEngine();
        mSchedules = new ArrayList<>();
        userID = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        mLayoutManager = new GridLayoutManager(ListScheduleActivity.this, 1);
        mView.setLayoutManager(mLayoutManager);
        mScheduleAdapter = new ScheduleAdapter(ListScheduleActivity.this, new ArrayList<Schedule>());
        mView.setAdapter(mScheduleAdapter);
        mView.addOnItemTouchListener(new RecyclerItemClickListener(ListScheduleActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        Intent i = new Intent(ListScheduleActivity.this, ListDetailScheduleActivity.class);
                        final Schedule selected = mScheduleAdapter.getSchedule(position);
                        i.putExtra("schedule", selected);
                        startActivity(i);
                    }
                })
        );
        getCars();
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void getCars() {
        showProgress(true);
        int userIDnum = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        final ArrayList<Car> theCars = new ArrayList<Car>();
        if (userIDnum >= 0) {
            mWashMyWhipEngine.getCars(userIDnum, new Callback<List<JSONObject>>() {
                @Override
                public void success(List<JSONObject> jsonObject, Response response) {
                    String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                    JSONArray mArray = null;
                    try {
                        mArray = new JSONArray(responseString);
                        for (int i = 0; i < mArray.length(); i++) {
                            JSONObject car = mArray.getJSONObject(i);
                            Car newCar = new Car(car);
                            theCars.add(newCar);
                            Log.d("getCars", " car: " + car.toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    showProgress(false);
                    mCars = theCars;
                    getSchedules();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("getCars", "error: " + error.toString());
                    showProgress(false);
                }
            });
        }
    }

    public void getSchedules() {
        showProgress(true);
        userID = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        final ArrayList<Schedule> theSchedules = new ArrayList<>();
        if (userID >= 0) {
            showProgress(true);
            mWashMyWhipEngine.getScheduledWashes(userID, new Callback<List<JSONObject>>() {
                @Override
                public void success(List<JSONObject> jsonObject, Response response) {
                    String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                    JSONArray mArray = null;
                    try {
                        mArray = new JSONArray(responseString);
                        for (int i = 0; i < mArray.length(); i++) {
                            JSONObject schedule = mArray.getJSONObject(i);
                            Schedule newSchedule = new Schedule(schedule, mCars);
                            theSchedules.add(newSchedule);
                            Log.d("getSchedules", " schedule: " + schedule.toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    showProgress(false);
                    Comparator<Schedule> comparador = Collections.reverseOrder();
                    Collections.sort(theSchedules, comparador);
                    mSchedules = theSchedules;
                    if (mSchedules != null) {
                        if (mSchedules.size() > 0) {
                            for (int i = 0; i < mSchedules.size(); i++) {
                                mScheduleAdapter.add(mSchedules.get(i));
                            }
                        } else {
                            mEmptyView.setVisibility(View.VISIBLE);
                            mLinearHeader.setVisibility(View.GONE);
                        }
                    } else {
                        mEmptyView.setVisibility(View.VISIBLE);
                        mLinearHeader.setVisibility(View.GONE);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("getSchedules", "failz: " + error.toString());
                    mEmptyView.setVisibility(View.VISIBLE);
                    mLinearHeader.setVisibility(View.GONE);
                    showProgress(false);
                }
            });
        }
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();
        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
}

