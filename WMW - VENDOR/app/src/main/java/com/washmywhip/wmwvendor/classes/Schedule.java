package com.washmywhip.wmwvendor.classes;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class Schedule implements Parcelable, Comparable<Schedule> {

    private int scheduleID;
    private int userID;
    private String date;
    private String startTime;
    private String endTime;
    private double lat;
    private double lng;
    private String locationName;
    private String locationStreet;
    private String locationCity;
    private String locationState;
    private String locationCountry;
    private String locationZip;
    private int washType;
    private int carID;
    private String comments;
    private String price;
    private Car car;

    public Schedule() {
    }

    public Schedule(Parcel in) {
        this.scheduleID = in.readInt();
        this.userID = in.readInt();
        this.date = in.readString();
        this.startTime = in.readString();
        this.endTime = in.readString();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
        this.locationName = in.readString();
        this.locationStreet = in.readString();
        this.locationState = in.readString();
        this.locationCountry = in.readString();
        this.locationZip = in.readString();
        this.washType = in.readInt();
        this.carID = in.readInt();
        this.comments = in.readString();
        this.price = in.readString();
        this.car = in.readParcelable(Car.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(scheduleID);
        parcel.writeInt(userID);
        parcel.writeString(date);
        parcel.writeString(startTime);
        parcel.writeString(endTime);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
        parcel.writeString(locationName);
        parcel.writeString(locationStreet);
        parcel.writeString(locationState);
        parcel.writeString(locationCountry);
        parcel.writeString(locationZip);
        parcel.writeInt(washType);
        parcel.writeInt(carID);
        parcel.writeString(comments);
        parcel.writeString(price);
        parcel.writeParcelable(car, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Schedule> CREATOR = new Creator<Schedule>() {

        public Schedule createFromParcel(Parcel in) {
            return new Schedule(in);
        }

        public Schedule[] newArray(int size) {
            return new Schedule[size];
        }
    };


    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationStreet() {
        return locationStreet;
    }

    public void setLocationStreet(String locationStreet) {
        this.locationStreet = locationStreet;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }

    public String getLocationState() {
        return locationState;
    }

    public void setLocationState(String locationState) {
        this.locationState = locationState;
    }

    public String getLocationCountry() {
        return locationCountry;
    }

    public void setLocationCountry(String locationCountry) {
        this.locationCountry = locationCountry;
    }

    public String getLocationZip() {
        return locationZip;
    }

    public void setLocationZip(String locationZip) {
        this.locationZip = locationZip;
    }

    public int getWashType() {
        return washType;
    }

    public void setWashType(int washType) {
        this.washType = washType;
    }

    public int getCarID() {
        return carID;
    }

    public void setCarID(int carID) {
        this.carID = carID;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public int getScheduleID() {
        return scheduleID;
    }

    public void setScheduleID(int scheduleID) {
        this.scheduleID = scheduleID;
    }

    public Schedule(JSONObject jsonData) {
        Map<String, Object> scheduleMap = new HashMap<>();
        try {
            scheduleMap = jsonToMap(jsonData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Object[] keyList = scheduleMap.keySet().toArray();
        for (Object key : keyList) {
            //Log.d("making schedule", "key: " + key.toString() + "  value: " + scheduleMap.get(key).toString());
        }

        this.setScheduleID(Integer.parseInt(scheduleMap.get("WashID").toString()));
        this.setUserID(Integer.parseInt(scheduleMap.get("UserID").toString()));
        this.setDate(scheduleMap.get("Date").toString());
        this.setStartTime(scheduleMap.get("StartTime").toString());
        this.setEndTime(scheduleMap.get("EndTime").toString());
        this.setLat(Double.parseDouble(scheduleMap.get("LocationLat").toString()));
        this.setLng(Double.parseDouble(scheduleMap.get("LocationLon").toString()));
        this.setLocationName(scheduleMap.get("LocationName").toString());
        this.setLocationStreet(scheduleMap.get("LocationStreet").toString());
        this.setLocationCity(scheduleMap.get("LocationCity").toString());
        this.setLocationState(scheduleMap.get("LocationState").toString());
        this.setLocationCountry(scheduleMap.get("LocationCountry").toString());
        this.setLocationZip(scheduleMap.get("LocationZIP").toString());
        this.setWashType(Integer.parseInt(scheduleMap.get("WashType").toString()));
        this.setCarID(Integer.parseInt(scheduleMap.get("CarID").toString()));
        this.setComments(scheduleMap.get("Comments").toString());
        this.setPrice(scheduleMap.get("Price").toString());
    }


    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    @Override
    public int compareTo(Schedule o) {
        String a = new String(String.valueOf(this.getDate()));
        String b = new String(String.valueOf(o.getDate()));
        return a.compareTo(b);
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
