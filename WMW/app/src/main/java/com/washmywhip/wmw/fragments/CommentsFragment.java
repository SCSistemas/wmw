package com.washmywhip.wmw.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.washmywhip.wmw.R;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class CommentsFragment extends DialogFragment {

    private EditText mEditComments;
    private Button mButtonSave;
    private Button mButtonCancel;
    private Typeface mFont;
    private Typeface mFontAvenir;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface CommentsFragmentDialogListener {
        public void onDialogComments(String comments);
    }

    private CommentsFragmentDialogListener mListener;

    public static CommentsFragment newInstance(CommentsFragmentDialogListener mListener) {

        CommentsFragment fragment = new CommentsFragment();
        fragment.mListener = mListener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_fragment_comments);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Avenir.ttc");
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mEditComments = (EditText) dialog.findViewById(R.id.dialog_comments);
        mEditComments.setTypeface(mFontAvenir);
        mButtonSave = (Button) dialog.findViewById(R.id.comments_save);
        mButtonCancel = (Button) dialog.findViewById(R.id.comments_cancel);
        mButtonSave.setTypeface(mFont);
        mButtonCancel.setTypeface(mFont);

        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mEditComments.getText().toString().isEmpty()) {
                    mListener.onDialogComments(mEditComments.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        return dialog;
    }
}


