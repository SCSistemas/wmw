package com.washmywhip.wmwvendor.util;

/**
 * Created by Ross on 3/14/2016.
 */
public enum VendorState {
    ACTIVE,INACTIVE,REQUESTING,NAVIGATING,ARRIVED,WASHING,FINALIZING
}
