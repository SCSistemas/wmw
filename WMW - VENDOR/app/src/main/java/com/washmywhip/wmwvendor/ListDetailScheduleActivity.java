package com.washmywhip.wmwvendor;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.washmywhip.wmwvendor.classes.Schedule;
import com.washmywhip.wmwvendor.services.WMWVendorEngine;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ListDetailScheduleActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {
    private Button completeButton;
    ImageView onBack;
    private View mProgressView;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private Schedule schedule;
    private String washName;
    private TextView mTextUser;
    private TextView mTextPhone;
    private TextView mTextWash;
    private TextView mTextCarModelColor;
    private TextView mTextCarPlate;
    private TextView mTextAdditonal;
    private TextView mTextComments;
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private LatLng currentLocation;
    private WMWVendorEngine mWMWVendorEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_detail_schedule);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        mWMWVendorEngine = new WMWVendorEngine();
        mProgressView = findViewById(R.id.progress_bar);
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        completeButton = (Button) findViewById(R.id.completeWash);
        completeButton.setOnClickListener(this);
        completeButton.setTypeface(mFont);
        mTextUser = (TextView) findViewById(R.id.detailScheduleUser);
        mTextUser.setTypeface(mFontAvenir);
        mTextPhone = (TextView) findViewById(R.id.detailSchedulePhone);
        mTextPhone.setTypeface(mFontAvenir);
        mTextWash = (TextView) findViewById(R.id.detailScheduleWash);
        mTextWash.setTypeface(mFontAvenir);
        mTextCarModelColor = (TextView) findViewById(R.id.detailScheduleCarMakeColor);
        mTextCarModelColor.setTypeface(mFontAvenir);
        mTextCarPlate = (TextView) findViewById(R.id.detailScheduleCarPlate);
        mTextCarPlate.setTypeface(mFontAvenir);
        mTextAdditonal = (TextView) findViewById(R.id.detailScheduleAdditional);
        mTextAdditonal.setTypeface(mFontAvenir);
        mTextComments = (TextView) findViewById(R.id.detailScheduleComments);
        mTextComments.setTypeface(mFontAvenir);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent i = getIntent();
        schedule = i.getParcelableExtra("schedule");
        getUser();
        getCar();
        mTextComments.setText(schedule.getComments());

        if (schedule.getWashType() == 0) {
            findCostWash();
        } else {
            if (schedule.getWashType() == 1) {
                findCostFull();
            } else {
                if (schedule.getWashType() == 2) {
                    findCostExtra();
                }
            }
        }
    }

    public void getUser() {
        showProgress(true);
        mWMWVendorEngine.getUserWithID(schedule.getUserID(), new Callback<Object>() {
            @Override
            public void success(Object object, Response response) {
                String s = object.toString();
                Log.d("getUser", s);
                String[] info = s.split(",");
                String first = info[2].replace(" FirstName=", "");
                String last = info[3].replace(" LastName=", "");
                String phone = info[5].replace(" Phone=", "");
                mTextUser.setText(first + " " + last);
                mTextPhone.setText(phone);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("getUser", error.toString());
                Toast.makeText(ListDetailScheduleActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                showProgress(false);
            }
        });
    }

    public void getCar() {
        showProgress(true);
        mWMWVendorEngine.getCarWithID(schedule.getCarID(), new Callback<Object>() {
            @Override
            public void success(Object object, Response response) {
                String s = object.toString();
                Log.d("getCar", s);
                String[] info = s.split(",");
                String make = info[3].replace(" Make=", "");
                String model = info[4].replace(" Model=", "");
                String color = info[2].replace(" Color=", "");
                String plate = info[5].replace(" Plate=", "");
                mTextCarModelColor.setText(make + " " + model + " (" + color + ")");
                mTextCarPlate.setText(plate.toUpperCase());
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("getUser", error.toString());
                Toast.makeText(ListDetailScheduleActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                showProgress(false);
            }
        });
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void findCostWash() {
        showProgress(true);
        final String body = "typeVal=0";
        RequestQueue queue = Volley.newRequestQueue(ListDetailScheduleActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        if (!response.contains(getResources().getString(R.string.error))) {
                            washName = "Fresh";
                            mTextWash.setText(washName + " Wash");
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ListDetailScheduleActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    public void findCostFull() {
        showProgress(true);
        final String body = "typeVal=1";
        RequestQueue queue = Volley.newRequestQueue(ListDetailScheduleActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        if (!response.contains(getResources().getString(R.string.error))) {
                            washName = "Luxe";
                            mTextWash.setText(washName + " Wash");
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    public void findCostExtra() {
        showProgress(true);
        final String body = "typeVal=2";
        RequestQueue queue = Volley.newRequestQueue(ListDetailScheduleActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        if (!response.contains(getResources().getString(R.string.error))) {
                            washName = "Royal";
                            mTextWash.setText(washName + " Wash");
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == completeButton.getId()) {
            if (schedule.getScheduleID() >= 0) {
                mWMWVendorEngine.completeTransaction(schedule.getScheduleID(), 0, "", new Callback<String>() {
                    @Override
                    public void success(String s, Response response) {
                        Log.d("completeTransaction", "success: " + s);
                        if (schedule.getUserID() > 0) {
                            mWMWVendorEngine.chargeStripeCustomer(schedule.getUserID(), Integer.parseInt(schedule.getPrice()), new Callback<String>() {
                                @Override
                                public void success(String s, Response response) {
                                    Log.d("chargeStripeCustomer", "success: " + s);
                                    Toast.makeText(ListDetailScheduleActivity.this, s, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Log.d("chargeStripeCustomer", "failz: " + error.toString());
                                }
                            });
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("completeTransaction", "failz: " + error.getMessage());
                    }
                });
            } else {
                Log.d("TransactionID", "ERROR GETTING TRANSACTION");
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.d("TEST", "map ready");
        mMap.setMyLocationEnabled(true);
        currentLocation = new LatLng(schedule.getLat(), schedule.getLng());
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 8.0f));
        mMap.addMarker(new MarkerOptions().position(currentLocation).title("Marker"));
    }
}
