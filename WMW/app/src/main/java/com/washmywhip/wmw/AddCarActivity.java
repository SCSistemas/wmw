package com.washmywhip.wmw;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.washmywhip.wmw.adapters.SpinnerAdapter;
import com.washmywhip.wmw.classes.Car;
import com.washmywhip.wmw.services.WashMyWhipEngine;
import com.washmywhip.wmw.util.Util;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class AddCarActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    private String carMake;
    private String carModel;
    private String carColor;
    private String carPlate;
    private int index = -1;
    static final int REQUEST_IMAGE_CAPTURE = 2;
    ImageView carImage;
    EditText colorEditText;
    Spinner brandDropdown;
    Spinner modelDropdown;
    EditText plateEditText;
    Button saveButton;
    String encodedCarImage;
    Map<String, String[]> carData = new HashMap<>();
    ArrayList<Car> mCars;
    private SharedPreferences mSharedPreferences;
    private WashMyWhipEngine mWashMyWhipEngine;
    private Typeface mFont;
    ImageView onBack;
    private View mProgressView;
    private boolean edit = false;
    private Car editCar;
    private int position = 0;
    private EditText[] fields;
    TextView.OnEditorActionListener mOnEditActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard(v);
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);
        mWashMyWhipEngine = new WashMyWhipEngine();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(AddCarActivity.this);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        carImage = (ImageView) findViewById(R.id.addCarPicture);
        colorEditText = (EditText) findViewById(R.id.addCarColor);
        brandDropdown = (Spinner) findViewById(R.id.addCarBrand);
        modelDropdown = (Spinner) findViewById(R.id.addCarModel);
        plateEditText = (EditText) findViewById(R.id.addCarPlate);
        saveButton = (Button) findViewById(R.id.saveCar);
        mProgressView = findViewById(R.id.progress_bar);
        colorEditText.setTypeface(mFont);
        plateEditText.setTypeface(mFont);
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        saveButton.setOnClickListener(this);
        saveButton.setTypeface(mFont);
        carImage.setOnClickListener(this);
        parseCarXML();
        mCars = new ArrayList<>();

        fields = new EditText[]{colorEditText, plateEditText};
        for (EditText field : fields) {
            field.setOnEditorActionListener(mOnEditActionListener);
        }

        Intent i = getIntent();
        editCar = i.getParcelableExtra("car");
        position = i.getIntExtra("position", -1);
        if (editCar != null) {
            edit = true;
            Picasso.with(AddCarActivity.this)
                    .load(getApplicationContext().getResources().getString(R.string.url_app) + "/CarImages/car" + editCar.getCarID() + "image.jpg")
                    .resize(100, 100)
                    .centerCrop()
                    .into(carImage);
            colorEditText.setText(editCar.getColor());
            plateEditText.setText(editCar.getPlate());
            carMake = editCar.getMake();
            carModel = editCar.getModel();
        }
        initMakeList();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == v.getId()) {
            for (EditText field : fields) {
                if (field.hasFocus()) {
                    hideKeyboard(field);
                }
            }
        }
        return true;
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void saveCar() {
        saveButton.setOnClickListener(null);
        carColor = colorEditText.getText().toString();
        carPlate = plateEditText.getText().toString();
        Util.hideKeyboard(colorEditText, AddCarActivity.this);
        Util.hideKeyboard(plateEditText, AddCarActivity.this);
        if (carColor == null || carPlate == null || carModel == null || carMake == null || carColor.equals("") || carPlate.equals("") || carModel.equals("") || carMake.equals("")) {
            Util.showAlertDialogDismiss(AddCarActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.enter_all_info_car));
        } else {
            String userID = mSharedPreferences.getString("UserID", null);
            if (userID != null) {
                showProgress(true);
                final int userIDnum = Integer.parseInt(userID);
                final Car newCar = new Car(userIDnum, carColor, carMake, carModel, carPlate);
                mWashMyWhipEngine.createCar(userIDnum, carColor, carMake, carModel, carPlate, encodedCarImage, new Callback<String>() {
                    @Override
                    public void success(String jsonObject, Response response) {
                        String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                        Log.d("Create Car", responseString);
                        EditText[] fields = {plateEditText, colorEditText};
                        for (EditText field : fields) {
                            if (field.hasFocus()) {
                                Util.hideKeyboard(field, AddCarActivity.this);
                            }
                        }
                        showProgress(false);
                        Toast.makeText(AddCarActivity.this, R.string.info_save, Toast.LENGTH_SHORT).show();
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("car", newCar);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("Create Car", error.toString());
                        saveButton.setOnClickListener(AddCarActivity.this);
                        Toast.makeText(AddCarActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                        showProgress(false);
                    }
                });
            }
        }
    }

    public void updateCar(final Car editCar) {
        saveButton.setOnClickListener(null);
        carColor = colorEditText.getText().toString();
        carPlate = plateEditText.getText().toString();
        Util.hideKeyboard(colorEditText, AddCarActivity.this);
        Util.hideKeyboard(plateEditText, AddCarActivity.this);
        if (carColor == null || carPlate == null || carModel == null || carMake == null || carColor.equals("") || carPlate.equals("") || carModel.equals("") || carMake.equals("")) {
            Util.showAlertDialogDismiss(AddCarActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.enter_all_info_car));
        } else {
            String userID = mSharedPreferences.getString("UserID", null);
            if (userID != null) {
                showProgress(true);
                final int userIDnum = Integer.parseInt(userID);
                final Car updateCar = new Car(editCar.getCarID(), userIDnum, carColor, carMake, carModel, carPlate);
                mWashMyWhipEngine.updateCar(editCar.getCarID(), userIDnum, carColor, carMake, carModel, carPlate, encodedCarImage, new Callback<String>() {
                    @Override
                    public void success(String jsonObject, Response response) {
                        String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                        Log.d("Create Car", responseString);
                        EditText[] fields = {plateEditText, colorEditText};
                        for (EditText field : fields) {
                            if (field.hasFocus()) {
                                Util.hideKeyboard(field, AddCarActivity.this);
                            }
                        }
                        showProgress(false);
                        Toast.makeText(AddCarActivity.this, R.string.info_save, Toast.LENGTH_SHORT).show();
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("car", updateCar);
                        returnIntent.putExtra("position", position);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("Create Car", error.toString());
                        saveButton.setOnClickListener(AddCarActivity.this);
                        Toast.makeText(AddCarActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                        showProgress(false);
                    }
                });
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == saveButton.getId()) {
            if (!edit) {
                saveCar();
            } else {
                updateCar(editCar);
            }
        } else if (v.getId() == carImage.getId()) {
            selectImage();
        }
    }

    public ArrayList<Car> readCarJSON(String s) {
        ArrayList<Car> list = new ArrayList<>();
        String[] elements = s.split("'}'");
        Log.d("CARS", "num cars: " + elements.length);
        return list;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == 1 && resultCode == RESULT_OK) {
                super.onActivityResult(requestCode, resultCode, data);
                Uri photoUri = data.getData();
                Bitmap selectedImage = null;
                byte[] byteArray = null;
                try {
                    selectedImage = Bitmap.createBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), photoUri));
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    byteArray = stream.toByteArray();
                    encodedCarImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    carImage.setImageBitmap(selectedImage);
                    saveButton.setOnClickListener(this);
                    saveButton.setBackgroundResource(R.drawable.rounded_corner_blue);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                    byte[] byteArray = null;
                    Bundle extras = data.getExtras();
                    Bitmap selectedImage = (Bitmap) extras.get("data");
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    byteArray = stream.toByteArray();
                    encodedCarImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    carImage.setImageBitmap(selectedImage);
                    saveButton.setOnClickListener(this);
                    saveButton.setBackgroundResource(R.drawable.rounded_corner_blue);
                }
            }
        }
    }

    private void selectImage() {
        final CharSequence[] items = {getResources().getString(R.string.take_photo), getResources().getString(R.string.chooose_library),
                getResources().getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(AddCarActivity.this);
        builder.setTitle(R.string.add_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getResources().getString(R.string.take_photo))) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else if (items[item].equals(getResources().getString(R.string.chooose_library))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 1);
                } else if (items[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void parseCarXML() {
        //Get Document Builder
        AssetManager assetManager = getAssets();
        InputStream instream = null;
        try {
            instream = assetManager.open("cars.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        //Build Document
        Document document = null;
        if (instream != null) {
            try {
                document = builder.parse(instream);
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Normalize the XML Structure; It's just too important !!
            document.getDocumentElement().normalize();

            NodeList mModelList = document.getElementsByTagName("carmodellist");
            NodeList mCarList = document.getElementsByTagName("carname");
            for (int i = 0; i < mModelList.getLength(); i++) {
                String info = mModelList.item(i).getTextContent();
                String key = mCarList.item(i).getTextContent();
                String[] elems = info.split("\n");
                for (int k = 0; k < elems.length; k++) {
                    elems[k] = elems[k].trim();
                    carData.put(key, elems);
                }
            }
        } else {
            Log.d("lolz", "inputstream is null");
        }
    }

    public void initMakeList() {
        String[] elems = carData.keySet().toArray(new String[carData.size()]);
        Arrays.sort(elems);
        final String[] items = elems;
        final ArrayAdapter adapterSpinner = new SpinnerAdapter(AddCarActivity.this, android.R.layout.simple_list_item_1, Color.parseColor("#CCCCCC"));
        adapterSpinner.addAll(elems);
        if (carMake != null) {
            adapterSpinner.add(carMake);
        } else {
            adapterSpinner.add(getResources().getString(R.string.brand));
        }
        brandDropdown.setAdapter(adapterSpinner);
        brandDropdown.setSelection(adapterSpinner.getCount());
        brandDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                index = position;
                carMake = (String) parent.getItemAtPosition(position);
                initModelList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });
    }

    public void initModelList() {
        if (index > 0) {
            String[] keySet = carData.keySet().toArray(new String[carData.size()]);
            Arrays.sort(keySet);
            final String[] items = carData.get(keySet[index - 1]);
            final String[] newItems = Arrays.copyOfRange(items, 1, items.length - 1);
            final ArrayAdapter adapterSpinner = new SpinnerAdapter(AddCarActivity.this, android.R.layout.simple_list_item_1, Color.parseColor("#CCCCCC"));
            adapterSpinner.addAll(newItems);
            if (carModel != null) {
                adapterSpinner.add(carModel);
            } else {
                adapterSpinner.add(getResources().getString(R.string.model));
            }
            modelDropdown.setAdapter(adapterSpinner);
            modelDropdown.setSelection(adapterSpinner.getCount());
            modelDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    carModel = (String) parent.getItemAtPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }

            });
        } else if (index == 0) {
            modelDropdown.setOnItemSelectedListener(null);
        }
    }
}
