package com.washmywhip.wmwvendor;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.washmywhip.wmwvendor.fragments.UpdatePasswordFragment;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, UpdatePasswordFragment.UpdatePasswordFragmentDialogListener {

    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private TextView forgotPassword;
    private Button logIn;
    private SharedPreferences mSharedPreferences;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private View mProgressView;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;

    private View.OnFocusChangeListener focusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                hideKeyboard(v);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        showAlertDialog();
        mUsernameView = (EditText) findViewById(R.id.usernameField);
        mUsernameView.setTypeface(mFontAvenir);
        forgotPassword = (TextView) findViewById(R.id.forgotPassword);
        forgotPassword.setOnClickListener(this);
        forgotPassword.setTypeface(mFont);
        logIn = (Button) findViewById(R.id.loginloginButton);
        logIn.setTypeface(mFont);
        logIn.setOnClickListener(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mPasswordView = (EditText) findViewById(R.id.passwordField);
        mUsernameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                    mPasswordView.requestFocus();
                    showKeyboard(mPasswordView);
                    return true;
                }
                return false;
            }
        });
        mPasswordView.setTypeface(mFontAvenir);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mPasswordView.setOnFocusChangeListener(focusChangeListener);
        mUsernameView.setOnFocusChangeListener(focusChangeListener);
        mProgressView = findViewById(R.id.progress_bar);
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void showAlertDialog() {
        dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_login, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        TextView mWelcome = (TextView) dialogView.findViewById(R.id.login_welcome);
        mWelcome.setTypeface(mFont);
        TextView mDesc = (TextView) dialogView.findViewById(R.id.login_desc);
        mDesc.setTypeface(mFontAvenir);
        TextView mText1 = (TextView) dialogView.findViewById(R.id.login_text1);
        mText1.setTypeface(mFontAvenir);
        TextView mText2 = (TextView) dialogView.findViewById(R.id.login_text2);
        mText2.setTypeface(mFontAvenir);
        LinearLayout linearWelcome = (LinearLayout) dialogView.findViewById(R.id.linear_dialog_welcome);
        linearWelcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void attemptLogin() {
        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            if (mPasswordView.hasFocus()) {
                hideKeyboard(mPasswordView);
            } else if (mUsernameView.hasFocus()) {
                hideKeyboard(mUsernameView);
            }
            loginWithVolley();
        }
    }

    public void loginWithVolley() {
        showProgress(true);
        final String body = "email=" + mUsernameView.getText().toString() +
                "&password=" + mPasswordView.getText().toString();
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        String url = "http://www.washmywhip.esy.es/requestVendorLogin.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        System.out.println("respuesta: " + response);
                        if (response.contains("Unsuccessful")) {
                            Toast.makeText(LoginActivity.this, "Datos Inválidos", Toast.LENGTH_LONG).show();
                        } else {
                            SharedPreferences.Editor prefsEditor = mSharedPreferences.edit();
                            prefsEditor.putString("email", mUsernameView.getText().toString()).apply();
                            prefsEditor.putString("password", mPasswordView.getText().toString()).apply();
                            prefsEditor.putBoolean("isLoggedIn", true).apply();
                            Map<String, String> userInfo = new HashMap<String, String>();
                            userInfo = parseResponse(response);
                            //if response tempPass = 1, prompt user for new password
                            String resetPass = (String) userInfo.get("isTempPass");
                            if (resetPass.equals("1")) {
                                UpdatePasswordFragment fragment = UpdatePasswordFragment.newInstance(LoginActivity.this);
                                fragment.show(getFragmentManager(), "update_password");
                            } else {
                                //LOGIN SUCCESS
                                SharedPreferences.Editor editor = mSharedPreferences.edit();
                                for (String s : userInfo.keySet()) {
                                    Log.d("thisismytag", s + " , " + userInfo.get(s));
                                    editor.putString(s, userInfo.get(s));
                                }
                                if (mPasswordView.hasFocus()) {
                                    hideKeyboard(mPasswordView);
                                }
                                if (mUsernameView.hasFocus()) {
                                    hideKeyboard(mUsernameView);
                                }
                                editor.commit();
                                Intent i = new Intent(LoginActivity.this, WashMyWhipActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            }
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                Toast.makeText(LoginActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    public void updateUserPasswordWithVolley(String newPass) {
        showProgress(true);
        final String newPassword = newPass;
        final String body = "email=" + mUsernameView.getText().toString() +
                "&password=" + newPassword;
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        String url = getResources().getString(R.string.url_app) + "/updateVendorPassword.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        System.out.println("respuesta: " + response);
                        if (response.contains("0")) {
                            Toast.makeText(LoginActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
                            mUsernameView.setText("");
                            mPasswordView.setText("");
                        } else if (response.contains("1")) {
                            Toast.makeText(LoginActivity.this, R.string.info_save, Toast.LENGTH_LONG).show();
                            Intent i = new Intent(LoginActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                Toast.makeText(LoginActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }
        };
        queue.add(sr);
    }

    public HashMap<String, String> parseResponse(String s) {
        HashMap userData = new HashMap();
        s = s.substring(1, s.length() - 1);
        s = s.replace(" ", "").replace("\t", "").replace(",", "").replace("\"", "");
        String[] dataItem = s.split("\n");
        for (int i = 1; i < dataItem.length; i++) {
            if (dataItem[i].endsWith(":")) {
                dataItem[i] = dataItem[i] + " ";
            }
            String[] info = dataItem[i].split(":");
            String key = info[0];
            String value = info[1];
            userData.put(key, value);
        }
        return userData;
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == logIn.getId()) {
            attemptLogin();
        } else if (v.getId() == forgotPassword.getId()) {
            Intent forgotPass = new Intent(this, ForgotPasswordActivity.class);
            startActivity(forgotPass);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 3;
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onDialogUpdatePassword(String password) {
        updateUserPasswordWithVolley(password);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }
}
