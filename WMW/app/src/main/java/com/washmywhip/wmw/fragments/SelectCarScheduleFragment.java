package com.washmywhip.wmw.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.washmywhip.wmw.AddCarActivity;
import com.washmywhip.wmw.R;
import com.washmywhip.wmw.adapters.CarAdapter;
import com.washmywhip.wmw.adapters.RecyclerItemClickListener;
import com.washmywhip.wmw.classes.Car;
import com.washmywhip.wmw.services.WashMyWhipEngine;
import com.washmywhip.wmw.util.SpacesItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class SelectCarScheduleFragment extends DialogFragment implements View.OnClickListener {

    private Typeface mFont;
    private ArrayList<Car> mCars;
    private GridLayoutManager mLayoutManager;
    private CarAdapter mCarAdapter;
    private WashMyWhipEngine mWashMyWhipEngine;
    RecyclerView mView;
    SharedPreferences mSharedPreferences;
    private Button mButtonSubmitCar;
    private Button mButtonCancelCar;
    private ArrayList<Car> theCars = new ArrayList<Car>();
    private Car carSelected;
    private View mProgressView;

    @Override
    public void onClick(View v) {

    }

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface SelectCarScheduleDialogListener {
        public void onSelectCarSchedule(Car car);
    }

    private SelectCarScheduleDialogListener mListener;

    public static SelectCarScheduleFragment newInstance(SelectCarScheduleDialogListener mListener) {
        SelectCarScheduleFragment fragment = new SelectCarScheduleFragment();
        fragment.mListener = mListener;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_fragment_select_car);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Archive.otf");
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mWashMyWhipEngine = new WashMyWhipEngine();
        mProgressView = dialog.findViewById(R.id.progress_bar);
        mView = (RecyclerView) dialog.findViewById(R.id.carGridView);
        mButtonSubmitCar = (Button) dialog.findViewById(R.id.update_password_submit);
        mButtonCancelCar = (Button) dialog.findViewById(R.id.update_password_cancel);
        mButtonSubmitCar.setTypeface(mFont);
        mButtonCancelCar.setTypeface(mFont);
        mLayoutManager = new GridLayoutManager(getActivity(), 1);
        mView.setLayoutManager(mLayoutManager);
        mCarAdapter = new CarAdapter(getActivity(), new ArrayList<Car>());
        mView.setAdapter(mCarAdapter);
        mView.addItemDecoration(new SpacesItemDecoration(8));
        mView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        carSelected = (Car) theCars.get(position);
                        Toast.makeText(getActivity(), "Car Selected: " + carSelected.getModel(), Toast.LENGTH_SHORT).show();
                    }
                })
        );
        //addCarsToView()
        Bundle bundle = getArguments();
        theCars = bundle.getParcelableArrayList("thecars");
        addCars();
        mButtonSubmitCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (carSelected != null) {
                    mListener.onSelectCarSchedule(carSelected);
                    dialog.dismiss();
                }
            }
        });

        mButtonCancelCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        return dialog;
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void addCars() {
        mCars = theCars;
        if (mCars != null) {
            if (mCars.size() > 0) {
                for (int i = 0; i < mCars.size(); i++) {
                    mCarAdapter.add(mCars.get(i));
                }
            }
        }
    }
}

