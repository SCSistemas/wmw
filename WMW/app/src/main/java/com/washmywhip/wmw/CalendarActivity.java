package com.washmywhip.wmw;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.andexert.calendarlistview.library.DayPickerView;
import com.andexert.calendarlistview.library.SimpleMonthAdapter;
import com.washmywhip.wmw.classes.Schedule;
import com.washmywhip.wmw.util.Util;
import com.washmywhip.wmw.util.WMW;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by enny.querales on 2/7/2016.
 */
public class CalendarActivity extends Activity implements com.andexert.calendarlistview.library.DatePickerController {

    private DayPickerView dayPickerView;
    private ImageView onBack;
    private Typeface mFont;
    private Button mButtonSelectDate;
    private String date = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        dayPickerView = (DayPickerView) findViewById(R.id.pickerView);
        dayPickerView.setController(this);
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mButtonSelectDate = (Button) findViewById(R.id.selectDate);
        mButtonSelectDate.setTypeface(mFont);
        mButtonSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!date.isEmpty()) {
                    Toast.makeText(CalendarActivity.this, "Date Selected: " + date, Toast.LENGTH_SHORT).show();
                    Schedule schedule = new Schedule();
                    schedule.setDate(date);
                    WMW.setSchedule(schedule);
                    Intent i = new Intent(CalendarActivity.this, ScheduleActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }

    @Override
    public int getMaxYear() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        return (calendar.get(Calendar.YEAR)+1);
    }

    @Override
    public void onDayOfMonthSelected(int year, int month, int day) {
        Log.e("Day Selected", day + " / " + month + " / " + year);
        SimpleMonthAdapter.SelectedDays selectedDays = dayPickerView.getSelectedDays();
        selectedDays.setLast(selectedDays.getFirst());
        GregorianCalendar newGregCal = new GregorianCalendar(year, month, day);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        date = df.format(newGregCal.getTime());
        if (Util.isBefore(new Date(), newGregCal.getTime())) {
            mButtonSelectDate.setVisibility(View.VISIBLE);
        } else {
            if (Util.isEquals(new Date(), newGregCal.getTime())) {
                mButtonSelectDate.setVisibility(View.VISIBLE);
            } else {
                mButtonSelectDate.setVisibility(View.GONE);
                Toast.makeText(CalendarActivity.this, R.string.date_expired, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDateRangeSelected(SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> selectedDays) {
        Log.e("Date range selected", selectedDays.getFirst().toString() + " --> " + selectedDays.getLast().toString());
        selectedDays.getLast().getDate();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        date = df.format(selectedDays.getLast().getDate());
        if (Util.isBefore(new Date(), selectedDays.getLast().getDate())) {
            mButtonSelectDate.setVisibility(View.VISIBLE);
        } else {
            if (Util.isEquals(new Date(), selectedDays.getLast().getDate())) {
                mButtonSelectDate.setVisibility(View.VISIBLE);
            } else {
                mButtonSelectDate.setVisibility(View.GONE);
                Toast.makeText(CalendarActivity.this, R.string.date_expired, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
