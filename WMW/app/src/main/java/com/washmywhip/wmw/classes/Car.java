package com.washmywhip.wmw.classes;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Car implements Parcelable{
    private int ownerID;
    private int carID;
    private String color;
    private String make;
    private String model;
    private String plate;

    public Car(int ownerID, String color, String make, String model, String plate) {
        this.setOwnerID(ownerID);
        this.setColor(color);
        this.setMake(make);
        this.setModel(model);
        this.setPlate(plate);
    }


    public Car(int carID, int ownerID, String color, String make, String model, String plate) {
        this.setOwnerID(ownerID);
        this.setCarID(carID);
        this.setColor(color);
        this.setMake(make);
        this.setModel(model);
        this.setPlate(plate);
    }

    public Car(Parcel in) {
        this.setOwnerID(in.readInt());
        this.setCarID(in.readInt());
        this.setColor(in.readString());
        this.setMake(in.readString());
        this.setModel(in.readString());
        this.setPlate(in.readString());
    }

    public Car(JSONObject jsonData) {
        Map<String, Object> carMap = new HashMap<>();
        try {
            carMap = jsonToMap(jsonData);
        } catch (JSONException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }

        Object[] keyList = carMap.keySet().toArray();
  /*      for (Object key : keyList) {
            Log.d("makingCAR", "key: " + key.toString() + "  value: " + carMap.get(key).toString());
        }*/

        this.setCarID(Integer.parseInt(carMap.get("CarID").toString()));
        this.setOwnerID(Integer.parseInt(carMap.get("Owner").toString()));
        this.setPlate(carMap.get("Plate").toString());
        this.setModel(carMap.get("Model").toString());
        this.setMake(carMap.get("Make").toString());
        this.setColor(carMap.get("Color").toString());
    }

    public int getCarID() {
        return carID;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public String getColor() {
        return color;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getPlate() {
        return plate;
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public static final Parcelable.Creator<Car> CREATOR = new Parcelable.Creator<Car>() {

        public Car createFromParcel(Parcel in) {
            return new Car(in);
        }

        public Car[] newArray(int size) {
            return new Car[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(getOwnerID());
        dest.writeInt(getCarID());
        dest.writeString(getColor());
        dest.writeString(getMake());
        dest.writeString(getModel());
        dest.writeString(getPlate());
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public void setCarID(int carID) {
        this.carID = carID;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }
}
