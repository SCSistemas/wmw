package com.washmywhip.wmw.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.washmywhip.wmw.R;

/**
 * Created by enny.querales on 27/4/2016.
 */
public class SpinnerAdapter extends ArrayAdapter<String> {

    private int color;
    private Typeface mFont;

    public SpinnerAdapter(Context context, int textViewResourceId, int color) {
        super(context, textViewResourceId);
        this.color = color;
        mFont = Typeface.createFromAsset(context.getAssets(), "fonts/Archive.otf");
// TODO Auto-generated constructor stub
    }

    @Override
    public int getCount() {
// TODO Auto-generated method stub
        int count = super.getCount();
        return count > 0 ? count - 1 : count;
    }

    // Affects default (closed) state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTextColor(getContext().getResources().getColor(R.color.color_dark_grey));
        view.setHintTextColor(color);
        view.setTextSize(20);
        view.setTypeface(mFont);
        view.setGravity(Gravity.CENTER);
        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTextSize(20);
        return view;
    }

    public void setCustomText(String customText) {
        // Call to set the text that must be shown in the spinner for the custom option.
        notifyDataSetChanged();
    }
}
