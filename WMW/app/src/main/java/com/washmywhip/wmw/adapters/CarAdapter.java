package com.washmywhip.wmw.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.washmywhip.wmw.R;
import com.washmywhip.wmw.classes.Car;
import com.washmywhip.wmw.util.RoundedImageView;

import java.util.List;

/**
 * Created by enny.querales on 27/6/2016.
 */

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {

    private Activity mActivity;
    private List<Car> result;
    private static ClickListener clickListener;
    private Typeface mFont;

    public CarAdapter(Activity context, List<Car> result) {
        this.result = result;
        this.mActivity = context;
        mFont = Typeface.createFromAsset(context.getAssets(), "fonts/Archive.otf");
    }

    @Override
    public CarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(final CarAdapter.ViewHolder holder, final int position) {
        final Car car = result.get(position);
        System.out.println("Car: " + car.getColor());
        holder.carColor.setText(car.getColor());
        holder.carColor.setTypeface(mFont);
        holder.carModel.setText(car.getMake() + " " + car.getModel());
        holder.carModel.setTypeface(mFont);
        holder.carPlate.setText(car.getPlate());
        holder.carPlate.setTypeface(mFont);
        holder.carID = car.getCarID();

        Picasso.with(mActivity)
                .load(mActivity.getResources().getString(R.string.url_app) + "/CarImages/car" + holder.carID + "image.jpg")
                .resize(100, 100)
                .centerCrop()
                .into(holder.carPic);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView carModel;
        public TextView carColor;
        public TextView carPlate;
        public RoundedImageView carPic;
        int carID;

        public ViewHolder(View mView) {
            super(mView);
            mView.setOnClickListener(this);
            carModel = (TextView) mView.findViewById(R.id.carModelProfile);
            carColor = (TextView) mView.findViewById(R.id.carColorProfile);
            carPlate = (TextView) mView.findViewById(R.id.carePlateProfile);
            carPic = (RoundedImageView) mView.findViewById(R.id.carPictureProfile);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        CarAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    @Override
    public int getItemCount() {
        return result.size();
    }

    public void add(Car car) {
        result.add(car);
        notifyItemInserted(result.size() - 1);
    }

    public void remove(int position) {
        result.remove(position);
        notifyItemRemoved(position);
    }

    public Car getCar(int position) {
        return result.get(position);
    }
}

