package com.washmywhip.wmw.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.washmywhip.wmw.R;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class WashingFragment extends DialogFragment {

    private SharedPreferences mSharedPreferences;
    private Typeface mFont;
    private Button contactButtonWashing;
    private LinearLayout mLinearContact;
    TextView textContact;
    TextView callContact;
    TextView doneContact;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface WashingFragmentDialogListener {
        public void onDialogWashing(String comments);
    }

    private WashingFragmentDialogListener mListener;

    public static WashingFragment newInstance(WashingFragmentDialogListener mListener) {

        WashingFragment fragment = new WashingFragment();
        fragment.mListener = mListener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        dialog.setContentView(R.layout.dialog_init_washing);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Archive.otf");
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mLinearContact = (LinearLayout) dialog.findViewById(R.id.linear_contact);
        String vendorName = mSharedPreferences.getString("vendorUsername", "Vendor Username");
        TextView vendorUserName = (TextView) dialog.findViewById(R.id.washingVendorName);
        vendorUserName.setTypeface(mFont);
        vendorUserName.setText(vendorName);

        contactButtonWashing = (Button) dialog.findViewById(R.id.washingContact);
        contactButtonWashing.setTypeface(mFont);
        contactButtonWashing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
      /*          mLinearContact.setVisibility(View.VISIBLE);
                initContact(v);*/
            }
        });

        dialog.show();
        return dialog;
    }

    private void initContact(View v) {
        textContact = (TextView) v.findViewById(R.id.contactText);
        textContact.setTypeface(mFont);
        textContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactTextVendor();
            }
        });
        callContact = (TextView) v.findViewById(R.id.contactCall);
        callContact.setTypeface(mFont);
        callContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactCallVendor();
            }
        });
        doneContact = (TextView) v.findViewById(R.id.contactDone);
        doneContact.setTypeface(mFont);
        doneContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initContact(v);
            }
        });
    }

    public void contactTextVendor() {
        String userNumber = mSharedPreferences.getString("vendorPhone", "null");
        if (!userNumber.equals("null")) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", userNumber, null)));
        }
    }

    public void contactCallVendor() {
        String userNumber = mSharedPreferences.getString("vendorPhone", "null");
        if (!userNumber.equals("null")) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + userNumber));
            startActivity(intent);
        }
    }
}


