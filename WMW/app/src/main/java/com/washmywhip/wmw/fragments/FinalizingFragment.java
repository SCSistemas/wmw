package com.washmywhip.wmw.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.washmywhip.wmw.R;
import com.washmywhip.wmw.classes.Schedule;
import com.washmywhip.wmw.services.WashMyWhipEngine;
import com.washmywhip.wmw.util.RoundedImageView;
import com.washmywhip.wmw.util.Util;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class FinalizingFragment extends DialogFragment {

    private SharedPreferences mSharedPreferences;
    private RoundedImageView finalizingVendorImage;
    Button finalizingSubmit;
    private RatingBar ratingBar;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private EditText finalizingComments;
    private WashMyWhipEngine mWashMyWhipEngine;
    private Schedule schedule;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface FinalizingFragmentDialogListener {
        public void onDialogFinalizing(int rating, String comments);
    }

    private FinalizingFragmentDialogListener mListener;

    public static FinalizingFragment newInstance(FinalizingFragmentDialogListener mListener) {

        FinalizingFragment fragment = new FinalizingFragment();
        fragment.mListener = mListener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_init_finalizing);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Avenir.ttc");
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mWashMyWhipEngine = new WashMyWhipEngine();
        finalizingSubmit = (Button) dialog.findViewById(R.id.finalizingSubmitButton);
        finalizingSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rating = ratingBar.getProgress();
                String comments = finalizingComments.getText().toString();
                if (finalizingComments.hasFocus()) {
                    Util.hideKeyboard(finalizingComments, getActivity());
                }

                if (schedule.getScheduleID() >= 0) {
                    mWashMyWhipEngine.rateVendor(schedule.getScheduleID(), rating, comments, new Callback<String>() {
                        @Override
                        public void success(String s, Response response) {
                            Log.d("rateVendor", "success: " + s);
                            Toast.makeText(getActivity(), R.string.success, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("rateVendor", "failz: " + error.getMessage());
                        }
                    });
                }
                mListener.onDialogFinalizing(rating, comments);
                dialog.dismiss();
            }
        });
        finalizingSubmit.setTypeface(mFont);

        Bundle bundle = getArguments();
        schedule = bundle.getParcelable("schedule");

        TextView howWouldYouRate = (TextView) dialog.findViewById(R.id.howRate);
        howWouldYouRate.setTypeface(mFont);

        final TextView vendorUsername = (TextView) dialog.findViewById(R.id.finalizingVendorName);
        vendorUsername.setTypeface(mFont);

        ratingBar = (RatingBar) dialog.findViewById(R.id.finalizingRating);
        finalizingComments = (EditText) dialog.findViewById(R.id.finalizingComments);
        finalizingComments.setTypeface(mFontAvenir);
        finalizingVendorImage = (RoundedImageView) dialog.findViewById(R.id.finalizingVendorImage);
        if (schedule.getVendorID() > 0) {
            Picasso.with(getActivity())
                    .load(getResources().getString(R.string.url_app) + "VendorAvatarImages/vendor" + schedule.getVendorID() + "avatar.jpg")
                    .resize(100, 100)
                    .centerCrop()
                    .into(finalizingVendorImage);
        }

        if (schedule.getVendorID() > 0) {
            mWashMyWhipEngine.getVendorWithID(schedule.getVendorID(), new Callback<Object>() {
                @Override
                public void success(Object object, Response response) {
                    String s = object.toString();
                    String[] info = s.split(",");
                    String username = info[1].replace(" Username=", "");
                    vendorUsername.setText(username);
                    //showProgress(false);
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getActivity(), R.string.error_try_again, Toast.LENGTH_SHORT).show();
                    //showProgress(false);
                }
            });
        }
        dialog.show();
        return dialog;
    }
}


