package com.washmywhip.wmwvendor.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.washmywhip.wmwvendor.R;

import java.util.ArrayList;


public class AboutListAdapter extends ArrayAdapter<String> {
    private Context context;
    private ArrayList<String> navDrawerItems;
    private int resourceID;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private SharedPreferences mSharedPreferences;


    public AboutListAdapter(Context context, int resource, ArrayList<String> navDrawerItems) {
        super(context, resource, navDrawerItems);
        mFont = Typeface.createFromAsset(context.getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        this.navDrawerItems = navDrawerItems;
        this.context = context;
        this.resourceID = resource;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public String getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();
            view = inflater.inflate(resourceID, parent, false);
            drawerHolder.ItemName = (TextView) view.findViewById(R.id.aboutTitle);
            view.setTag(drawerHolder);
        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();

        }

        String item = this.navDrawerItems.get(position);
        drawerHolder.ItemName.setText(item);
        drawerHolder.ItemName.setTypeface(mFontAvenir);
        return view;
    }

    private static class DrawerItemHolder {
        TextView ItemName;
        ImageView icon;
    }
}
