package com.washmywhip.wmwvendor.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by enny.querales on 12/6/2016.
 */
public class Util {

    public static boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public static boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 3;
    }


    public static String hashPassword(String password) {
        String hashword = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(password.getBytes());
            BigInteger hash = new BigInteger(1, md5.digest());
            hashword = hash.toString(16);
        } catch (NoSuchAlgorithmException nsae) {

        }
        return pad(hashword, 32, '0');
    }

    public static String pad(String s, int length, char pad) {
        StringBuffer buffer = new StringBuffer(s);
        while (buffer.length() < length) {
            buffer.insert(0, pad);
        }
        return buffer.toString();
    }

    /**
     * Show errors to users
     */
    private static void showErrorDialog(String message, Context context) {
        new AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static String getPath(Activity mContext, Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = mContext.managedQuery(uri, projection, null, null, null);
        cursor.moveToFirst();
        String filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
        return filePath;
    }

    /**
     * Escala el bitmap (Aumenta o reduce) segun el maximo Ancho y maximo Alto recibido
     *
     * @param mUri     Uri de la imagen normalmente obtenida intent.getData
     * @param mWidth   Maximo Ancho permitido para la imagen
     * @param mHeight  Maximo Alto permitido para la imagen
     * @param mContext Contexto de donde se realiza el llamado
     * @return Bitmap escalado
     */
    public static Bitmap scalateBitmap(Context mContext, Uri mUri, int mWidth, int mHeight) {
        Bitmap scaled = null;
        try {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inJustDecodeBounds = true;
            InputStream input = mContext.getContentResolver().openInputStream(
                    mUri);
            BitmapFactory.decodeStream(input, null, opts);
            int width = opts.outWidth;
            int height = opts.outHeight;
            double scaleFactor = (width > height) ? ((double) mWidth / width)
                    : ((double) mHeight / height);
            int newHeight = Math.round((float) (height * scaleFactor));
            int newWidth = Math.round((float) (width * scaleFactor));
            int scale = calculateInSampleSize(opts, newWidth, newHeight);
            input.close();
            opts.inJustDecodeBounds = false;
            opts.inTempStorage = new byte[16 * 1024];
            opts.inSampleSize = scale;
            input = mContext.getContentResolver().openInputStream(
                    mUri);
            Bitmap bitmap = BitmapFactory.decodeStream(input, null, opts);
            //int nw = (int) (bitmap.getWidth()  * (metrics.heightPixels / bitmap.getHeight()) );
            scaled = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
        } catch (FileNotFoundException fe) {
            System.out.println(fe.getMessage());
        } catch (IOException fe) {
            System.out.println(fe.getMessage());
        }

        return scaled;
    }


    /**
     * Calcula el tamaño indicado para el inSampleSize dependiendo del Ancho y Alto maximo recibido para que mantenga su escala (no se distorsione)
     *
     * @param options   options tomados por el bitmap
     * @param reqWidth  Ancho maximo
     * @param reqHeight Alto Maximo
     * @return entero que indica cuanto se puede reducir
     */
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String StringToMd5(String text) {

        MessageDigest md;
        StringBuffer sb = new StringBuffer();
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(text.getBytes("UTF-8"));

            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
                        .substring(1, 3));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        }

        return sb.toString();

    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else
            return false;
    }

    public static String getTimeFormat(Date time, char format) {
        String fmt = "";
        if (time == null) {
            fmt = "null";
        } else {
            switch (format) {
                case 'M':
                    fmt = DateFormat.getTimeInstance(DateFormat.MEDIUM).format(time);
                    break;
                case 'L':
                    fmt = DateFormat.getTimeInstance(DateFormat.LONG).format(time);
                    break;
                case 'S':
                    fmt = DateFormat.getTimeInstance(DateFormat.SHORT).format(time);
                    break;
            }
        }
        return fmt;
    }

    public static double applyFormat(double number) {
        //Dividir numero; entera y decimales.
        double entera = new Double(number).intValue();
        double decimales = number - entera;
        //Redondeamos la parte decimal.
        BigDecimal numero = new BigDecimal(decimales, MathContext.DECIMAL32).setScale(2, BigDecimal.ROUND_HALF_UP);
        //Preparar el formato.
        NumberFormat editFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);//NumberFormat Ingles (separador decimal punto)
        editFormat.setMaximumFractionDigits(2);//Decimales DOS.
        editFormat.setGroupingUsed(false);//Sin separador de miles.
        return Double.valueOf(editFormat.format(entera + numero.doubleValue())).doubleValue();
    }

    public static double distance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 3958.75; // in miles, change to 6371 for kilometer output
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;
        return dist; // output distance, in MILES
    }

    public static void hideKeyboard(View view, Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard(View view, Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public static Date getHour(String hour) {
        DateFormat df = new SimpleDateFormat("HH:mm");
        Date parsed = null;
        try {
            parsed = df.parse(hour);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsed;
    }
}
