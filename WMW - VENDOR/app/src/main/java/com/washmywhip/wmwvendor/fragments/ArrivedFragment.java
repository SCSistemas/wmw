package com.washmywhip.wmwvendor.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.washmywhip.wmwvendor.R;
import com.washmywhip.wmwvendor.util.RoundedImageView;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class ArrivedFragment extends DialogFragment {
    private Typeface mFont;
    private Button takeBeforePictureArrived;
    private SharedPreferences mSharedPreferences;
    private static final int BEFORE_REQUEST = 1647;
    private RelativeLayout arrivedcontact;
    private Button beginWashArrived;
    private RoundedImageView userCarImageArrived;
    private LinearLayout mLinearContact;
    TextView textContact;
    TextView callContact;
    TextView doneContact;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface ArrivedFragmentDialogListener {
        public void onDialogArrived(String comments);
    }

    private ArrivedFragmentDialogListener mListener;

    public static ArrivedFragment newInstance(ArrivedFragmentDialogListener mListener) {

        ArrivedFragment fragment = new ArrivedFragment();
        fragment.mListener = mListener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_init_arrived);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Archive.otf");
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        // mLinearContact = (LinearLayout) dialog.findViewById(R.id.linear_contact);
        takeBeforePictureArrived = (Button) dialog.findViewById(R.id.arrivedBeforePicture);
        takeBeforePictureArrived.setTypeface(mFont);
        takeBeforePictureArrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //selectImage(BEFORE_REQUEST);
            }
        });
/*        arrivedcontact = (RelativeLayout) dialog.findViewById(R.id.arrivedContact);
        arrivedcontact.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //initContact(v);
                    arrivedcontact.setOnClickListener(null);
                    Log.d("contact", "test");
                    return true;
                }
                return false;
            }
        });*/

        beginWashArrived = (Button) dialog.findViewById(R.id.arrivedBeginWash);
        beginWashArrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDialogArrived("");
                dialog.dismiss();
            }
        });
        beginWashArrived.setTypeface(mFont);

        TextView carColorArrived = (TextView) dialog.findViewById(R.id.arrivedCarColor);
        TextView carMakeAndModlArrived = (TextView) dialog.findViewById(R.id.arrivedCarMakeAndModel);
        TextView carPlateArrived = (TextView) dialog.findViewById(R.id.arrivedCarPlate);
        TextView userNameArrived = (TextView) dialog.findViewById(R.id.arrivedUserName);
        TextView youHaveArrived = (TextView) dialog.findViewById(R.id.arrivedYouHaveArrived);
        TextView washType = (TextView) dialog.findViewById(R.id.arrivedWashType);
        TextView washCost = (TextView) dialog.findViewById(R.id.arrivedWashCost);

        youHaveArrived.setTypeface(mFont);
        washType.setTypeface(mFont);
        washCost.setTypeface(mFont);

        String color = mSharedPreferences.getString("CarColor", "null");
        String make = mSharedPreferences.getString("CarMake", "null");
        String model = mSharedPreferences.getString("CarModel", "null");
        String plate = mSharedPreferences.getString("CarPlate", "null");
        String name = mSharedPreferences.getString("userFullName", "null");
        int costOfWash = mSharedPreferences.getInt("washPrice", 20);
        washCost.setText("$" + costOfWash + ".00");
        String makeAndModel = make + " " + model;

        carColorArrived.setText(color);
        carColorArrived.setTypeface(mFont);
        carMakeAndModlArrived.setText(makeAndModel);
        carMakeAndModlArrived.setTypeface(mFont);
        carPlateArrived.setText(plate);
        carPlateArrived.setTypeface(mFont);
        userNameArrived.setText(name);
        userNameArrived.setTypeface(mFont);


        int carID = mSharedPreferences.getInt("carID", -1);
        userCarImageArrived = (RoundedImageView) dialog.findViewById(R.id.arrivedUserPicture);
        if (carID > 0) {
            Picasso.with(getActivity())
                    .load("http://www.WashMyWhip.us/wmwapp/CarImages/car" + carID + "image.jpg")
                    .resize(100, 100)
                    .centerCrop()
                    .into(userCarImageArrived);
        }

        dialog.show();
        return dialog;
    }

    private void selectImage(final int requestCode) {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //0 is request code
                    startActivityForResult(intent, requestCode);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), requestCode);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

/*    private void initContact(View v) {
        textContact = (TextView) v.findViewById(R.id.contactText);
        textContact.setTypeface(mFont);
        textContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactTextVendor();
            }
        });
        callContact = (TextView) v.findViewById(R.id.contactCall);
        callContact.setTypeface(mFont);
        callContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactCallVendor();
            }
        });
        doneContact = (TextView) v.findViewById(R.id.contactDone);
        doneContact.setTypeface(mFont);
        doneContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }*/

    public void contactTextVendor() {
        String userNumber = mSharedPreferences.getString("vendorPhone", "null");
        if (!userNumber.equals("null")) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", userNumber, null)));
        }
    }

    public void contactCallVendor() {
        String userNumber = mSharedPreferences.getString("vendorPhone", "null");
        if (!userNumber.equals("null")) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + userNumber));
            startActivity(intent);
        }
    }
}


