package com.washmywhip.wmw;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.devmarvel.creditcardentry.library.CreditCard;
import com.devmarvel.creditcardentry.library.CreditCardForm;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;
import com.washmywhip.wmw.services.WashMyWhipEngine;
import com.washmywhip.wmw.util.Util;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class AddPaymentActivity extends AppCompatActivity implements View.OnClickListener {
    private Button saveButton;
    private LinearLayout linearLayout;
    private CreditCardForm form;
    private SharedPreferences mSharedPreferences;
    private WashMyWhipEngine mWashMyWhipEngine;
    private int userID;
    ImageView onBack;
    private Typeface mFont;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(AddPaymentActivity.this);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        userID = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        mWashMyWhipEngine = new WashMyWhipEngine();
        form = new CreditCardForm(AddPaymentActivity.this);
        linearLayout = (LinearLayout) findViewById(R.id.paymentFrag);
        linearLayout.addView(form);
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        saveButton = (Button) findViewById(R.id.addPayment);
        saveButton.setOnClickListener(this);
        saveButton.setTypeface(mFont);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == saveButton.getId()) {
            if (form.isCreditCardValid()) {
                addPayment();
            } else {
                Util.showAlertDialogDismiss(AddPaymentActivity.this, getResources().getString(R.string.error), getResources().getString(R.string.enter_valid_card));
            }
        }
    }

    public void addPayment() {
        CreditCard card = form.getCreditCard();
        com.stripe.android.model.Card stripeCard = new com.stripe.android.model.Card(card.getCardNumber().replace(" ", ""), card.getExpMonth(), card.getExpYear(), card.getSecurityCode());
        //Pass credit card to service
        Log.d("addPayment", "" + card.getCardNumber());
        if (userID >= 0) {
            if (stripeCard.validateCard() && stripeCard.validateCVC()) {
                try {
                    Stripe stripe = new Stripe("pk_test_3YnnEvlBjBWoysy0JcuKDM72");
                    stripe.createToken(stripeCard, new TokenCallback() {
                        @Override
                        public void onError(Exception error) {
                            Log.d("Stripe", "Create token failz: " + error.toString());
                        }

                        @Override
                        public void onSuccess(Token token) {
                            Log.d("Stripe", "Create token success for userID = " + userID + " " + token);
                            //send token to server
                            mWashMyWhipEngine.addPaymentSource(userID, token.getId(), new Callback<Object>() {
                                @Override
                                public void success(Object o, Response response) {
                                    final String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                                    Log.d("Stripe", "Sending token to server success: " + responseString);
                                    final AlertDialog.Builder builder = new AlertDialog.Builder(AddPaymentActivity.this);
                                    builder.setTitle(getResources().getString(R.string.info_save));
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent returnIntent = new Intent();
                                            setResult(Activity.RESULT_OK, returnIntent);
                                            finish();
                                            dialog.cancel();
                                        }
                                    });
                                    builder.show();
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Log.d("Add Payment", error.toString());
                                    Toast.makeText(AddPaymentActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                } catch (AuthenticationException e) {
                    e.printStackTrace();
                    Toast.makeText(AddPaymentActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
