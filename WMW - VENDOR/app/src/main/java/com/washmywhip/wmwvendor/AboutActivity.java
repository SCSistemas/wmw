package com.washmywhip.wmwvendor;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.washmywhip.wmwvendor.adapter.AboutListAdapter;

import java.util.ArrayList;

public class AboutActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    ListView aboutList;
    ImageView onBack;
    private Typeface mFont;
    private TextView mTextAbout;

    public AboutActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ArrayList<String> data = new ArrayList<>();
        data.add("Rate Us In The App Store");
        data.add("Like Us On Facebook");
        data.add("Our Website");
        data.add("Legal");
        aboutList = (ListView) findViewById(R.id.aboutList);
        aboutList.setAdapter(new AboutListAdapter(AboutActivity.this, R.layout.about_item, data));
        aboutList.setOnItemClickListener(this);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mTextAbout = (TextView) findViewById(R.id.title_about);
        mTextAbout.setTypeface(mFont);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(position == 0){
            Log.d("AboutFrag","0");
            rateOnPlayStore();
        } else if (position == 1){
            Log.d("AboutFrag","1");
            likeUsOnFacebook();
        } else if (position == 2){
            Log.d("AboutFrag","2");
            ourWebsite();
        } else if (position == 3){
            Log.d("AboutFrag","3");
            legalDocumentation();
        } else if (position == 4){
            Log.d("AboutFrag","4");
        }

    }

    public void rateOnPlayStore(){
        Uri uri = Uri.parse("market://details?id=com.washmywhip.wmwpartner");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }

    public void likeUsOnFacebook(){
        String url = "http://www.facebook.com";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void ourWebsite() {
        String url = "http://www.washmywhip.com";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void legalDocumentation(){
        String url = "http://www.washmywhip.com/legal";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void onClick(View v) {

    }
}
