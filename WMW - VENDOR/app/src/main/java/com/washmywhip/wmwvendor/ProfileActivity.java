package com.washmywhip.wmwvendor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.method.KeyListener;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.washmywhip.wmwvendor.services.WMWVendorEngine;
import com.washmywhip.wmwvendor.util.RoundedImageView;
import com.washmywhip.wmwvendor.util.Util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private String username, email, phone, encodedProfile;
    private int vendorID;
    private WMWVendorEngine mWMWVendorEngine;
    SharedPreferences mSharedPreferences;
    RoundedImageView profilePicture;
    EditText usernameProfile;
    EditText emailEditText;
    EditText phoneEditText;
    TextView editButton;
    KeyListener defaultKeyListener;
    TextView account;
    ImageView onBack;
    private View mProgressView;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private Button mFinishedSession;
    static final int REQUEST_IMAGE_CAPTURE = 2;

    public ProfileActivity() {
        // Required empty public constructor
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void initEditable() {
        editButton.setText("Save");
        profilePicture.setOnClickListener(this);
        usernameProfile.setKeyListener(defaultKeyListener);
        usernameProfile.setEnabled(true);
        emailEditText.setKeyListener(defaultKeyListener);
        emailEditText.setEnabled(true);
        phoneEditText.setKeyListener(defaultKeyListener);
        phoneEditText.setEnabled(true);
        EditText[] fields = {usernameProfile, emailEditText, phoneEditText};

        for (EditText field : fields) {
            if (field.hasFocus()) {
                Util.hideKeyboard(field, ProfileActivity.this);
            }
        }
    }

    public void initNotEditable() {
        editButton.setText("Edit");
        usernameProfile.setActivated(false);
        usernameProfile.setKeyListener(null);
        usernameProfile.setEnabled(false);
        emailEditText.setKeyListener(null);
        emailEditText.setEnabled(false);
        phoneEditText.setKeyListener(null);
        phoneEditText.setEnabled(false);
        profilePicture.setOnClickListener(null);
        username = usernameProfile.getText().toString();
        email = emailEditText.getText().toString();
        phone = phoneEditText.getText().toString();
        final AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(getResources().getString(R.string.hold_on));
        builder.setMessage(getResources().getString(R.string.sure_save_info_profile));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int vendorID = Integer.parseInt(mSharedPreferences.getString("VendorID", "-1"));
                showProgress(true);
                mWMWVendorEngine.updateVendorInfo(vendorID, email, username, phone, new Callback<Object>() {
                    @Override
                    public void success(Object o, Response response) {
                        mSharedPreferences.edit().putString("Username", username).commit();
                        mSharedPreferences.edit().putString("Email", email).commit();
                        mSharedPreferences.edit().putString("Phone", phone).commit();
                        showProgress(false);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(ProfileActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                        showProgress(false);
                    }
                });
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mWMWVendorEngine = new WMWVendorEngine();
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        profilePicture = (RoundedImageView) findViewById(R.id.pictureProfile);
        usernameProfile = (EditText) findViewById(R.id.firstNameProfile);
        emailEditText = (EditText) findViewById(R.id.emailProfile);
        phoneEditText = (EditText) findViewById(R.id.phoneProfile);
        editButton = (TextView) findViewById(R.id.editButton);
        account = (TextView) findViewById(R.id.accountProfile);
        mProgressView = findViewById(R.id.progress_bar);
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mFinishedSession = (Button) findViewById(R.id.finished_session);
        mFinishedSession.setTypeface(mFont);
        mFinishedSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogout();
            }
        });
        editButton.setOnClickListener(this);
        profilePicture.setOnClickListener(null);
        editButton.setTypeface(mFontAvenir);
        account.setTypeface(mFont);
        account.setTypeface(mFont);
        usernameProfile.setTypeface(mFont);
        emailEditText.setTypeface(mFont);
        phoneEditText.setTypeface(mFont);
        showProgress(true);
        vendorID = Integer.parseInt(mSharedPreferences.getString("VendorID", "-1"));
        Picasso.with(ProfileActivity.this)
                .load(getResources().getString(R.string.url_app) + "/VendorAvatarImages/vendor" + vendorID + "avatar.jpg")
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .resize(200, 200)
                .centerCrop().placeholder(R.drawable.picture).error(R.drawable.picture).into(profilePicture);
        mWMWVendorEngine.getVendorWithID(vendorID, new Callback<Object>() {
            @Override
            public void success(Object object, Response response) {
                String s = object.toString();
                String[] info = s.split(",");
                username = info[1].replace(" Username=", "");
                email = info[2].replace(" Email=", "");
                phone = info[3].replace(" Phone=", "");
                usernameProfile.setText(username);
                emailEditText.setText(email);
                phoneEditText.setText(phone);
                defaultKeyListener = usernameProfile.getKeyListener();
                usernameProfile.setActivated(false);
                usernameProfile.setKeyListener(null);
                usernameProfile.setEnabled(false);
                usernameProfile.setKeyListener(null);
                usernameProfile.setEnabled(false);
                emailEditText.setKeyListener(null);
                emailEditText.setEnabled(false);
                phoneEditText.setKeyListener(null);
                phoneEditText.setEnabled(false);
                showProgress(false);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ProfileActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                showProgress(false);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == editButton.getId()) {
            if (editButton.getText().toString().equals("Save")) {
                initNotEditable();
            } else if (editButton.getText().toString().equals("Edit")) {
                initEditable();
            }
        } else if (v.getId() == profilePicture.getId()) {
            selectImage();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == 1 && resultCode == RESULT_OK) {
                super.onActivityResult(requestCode, resultCode, data);
                Uri photoUri = data.getData();
                Bitmap selectedImage = null;
                byte[] byteArray = null;
                try {
                    showProgress(true);
                    selectedImage = Bitmap.createBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), photoUri));
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                    byteArray = stream.toByteArray();
                    encodedProfile = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    profilePicture.setImageBitmap(selectedImage);
                    mWMWVendorEngine.uploadVendorAvatarImageAndroid(vendorID, encodedProfile, new Callback<Object>() {
                        @Override
                        public void success(Object s, Response response) {
                            Log.d("vendorAvatarUpload", "Success " + s.toString());
                            Toast.makeText(ProfileActivity.this, R.string.photo_upload, Toast.LENGTH_SHORT).show();
                            showProgress(false);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("vendorAvatarUpload", "error: " + error.getMessage());
                            Toast.makeText(ProfileActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                            showProgress(false);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                    byte[] byteArray = null;
                    showProgress(true);
                    Bundle extras = data.getExtras();
                    Bitmap selectedImage = (Bitmap) extras.get("data");
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                    byteArray = stream.toByteArray();
                    encodedProfile = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    profilePicture.setImageBitmap(selectedImage);
                    mWMWVendorEngine.uploadVendorAvatarImageAndroid(vendorID, encodedProfile, new Callback<Object>() {
                        @Override
                        public void success(Object s, Response response) {
                            Log.d("vendorAvatarUpload", "Success " + s.toString());
                            Toast.makeText(ProfileActivity.this, R.string.photo_upload, Toast.LENGTH_SHORT).show();
                            showProgress(false);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("vendorAvatarUpload", "error: " + error.getMessage());
                            Toast.makeText(ProfileActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                            showProgress(false);
                        }
                    });
                }
            }
        }
    }

    private void selectImage() {
        final CharSequence[] items = {getResources().getString(R.string.take_photo), getResources().getString(R.string.chooose_library),
                getResources().getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(R.string.add_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getResources().getString(R.string.take_photo))) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else if (items[item].equals(getResources().getString(R.string.chooose_library))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 1);
                } else if (items[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void attemptLogout() {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.sign_out));
        builder.setMessage(getResources().getString(R.string.sure_sign_out));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mSharedPreferences.edit().clear().commit();
                mSharedPreferences.edit().putBoolean("isLoggedIn", false).commit();
                Intent i = new Intent(ProfileActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
}
