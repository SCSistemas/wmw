package com.washmywhip.wmw.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.washmywhip.wmw.R;
import com.washmywhip.wmw.util.RoundedImageView;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class WaitingFragment extends DialogFragment {
    private RelativeLayout waitingContactLayout;
    private SharedPreferences mSharedPreferences;
    private RoundedImageView vendorWaitingImage;
    private LinearLayout mLinearContact;
    TextView textContact;
    TextView callContact;
    TextView doneContact;
    private Typeface mFont;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface WaitingFragmentDialogListener {
        public void onDialogWaiting(String comments);
    }

    private WaitingFragmentDialogListener mListener;

    public static WaitingFragment newInstance(WaitingFragmentDialogListener mListener) {

        WaitingFragment fragment = new WaitingFragment();
        fragment.mListener = mListener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_init_waiting);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Archive.otf");
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mLinearContact = (LinearLayout) dialog.findViewById(R.id.linear_contact);
        waitingContactLayout = (RelativeLayout) dialog.findViewById(R.id.waitingContactLayout);
        waitingContactLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
     /*           mLinearContact.setVisibility(View.VISIBLE);
                initContact(v);*/
            }
        });
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String vendorName = mSharedPreferences.getString("vendorUsername", "Vendor Username");
        TextView vendorUsername = (TextView) dialog.findViewById(R.id.waitingVendorName);
        vendorUsername.setTypeface(mFont);
        vendorUsername.setText(vendorName);

        int vendorID = mSharedPreferences.getInt("vendorID", -1);
        vendorWaitingImage = (RoundedImageView) dialog.findViewById(R.id.waitingPicture);
        if (vendorID > 0) {
            Picasso.with(getActivity())
                    .load("http://www.WashMyWhip.us/wmwapp/VendorAvatarImages/vendor" + vendorID + "avatar.jpg")
                    .resize(100, 100)
                    .centerCrop()
                    .into(vendorWaitingImage);
        }

        dialog.show();
        return dialog;
    }

    private void initContact(View v) {
        textContact = (TextView) v.findViewById(R.id.contactText);
        textContact.setTypeface(mFont);
        textContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactTextVendor();
            }
        });
        callContact = (TextView) v.findViewById(R.id.contactCall);
        callContact.setTypeface(mFont);
        callContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactCallVendor();
            }
        });
        doneContact = (TextView) v.findViewById(R.id.contactDone);
        doneContact.setTypeface(mFont);
        doneContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void contactTextVendor() {
        String userNumber = mSharedPreferences.getString("vendorPhone", "null");
        if (!userNumber.equals("null")) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", userNumber, null)));
        }
    }

    public void contactCallVendor() {
        String userNumber = mSharedPreferences.getString("vendorPhone", "null");
        if (!userNumber.equals("null")) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + userNumber));
            startActivity(intent);
        }
    }
}


