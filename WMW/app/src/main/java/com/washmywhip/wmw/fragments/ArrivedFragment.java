package com.washmywhip.wmw.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.washmywhip.wmw.R;
import com.washmywhip.wmw.util.RoundedImageView;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class ArrivedFragment extends DialogFragment {

    private Button contactButtonArrived;
    private Typeface mFont;
    private SharedPreferences mSharedPreferences;
    private RoundedImageView arrivedVendorImage;
    private LinearLayout mLinearContact;
    TextView textContact;
    TextView callContact;
    TextView doneContact;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface ArrivedFragmentDialogListener {
        public void onDialogArrived(String comments);
    }

    private ArrivedFragmentDialogListener mListener;

    public static ArrivedFragment newInstance(ArrivedFragmentDialogListener mListener) {

        ArrivedFragment fragment = new ArrivedFragment();
        fragment.mListener = mListener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_init_arrived);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Archive.otf");
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mLinearContact = (LinearLayout) dialog.findViewById(R.id.linear_contact);
        contactButtonArrived = (Button) dialog.findViewById(R.id.arrivedContact);
        contactButtonArrived.setTypeface(mFont);
        contactButtonArrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearContact.setVisibility(View.VISIBLE);
                initContact(v);
            }
        });
        TextView arrivedVendorUsername = (TextView) dialog.findViewById(R.id.arrivedVendorUsername);
        TextView arrivedWashCost = (TextView) dialog.findViewById(R.id.arrivedWashCost);
        TextView arrivedWashType = (TextView) dialog.findViewById(R.id.arrivedWashType);
        TextView hasArrived = (TextView) dialog.findViewById(R.id.hasArrived);
        hasArrived.setTypeface(mFont);
        arrivedVendorUsername.setTypeface(mFont);
        arrivedWashCost.setTypeface(mFont);
        arrivedWashType.setTypeface(mFont);
        String vendorName = mSharedPreferences.getString("vendorUsername", "Vendor Username");
        arrivedVendorUsername.setText(vendorName);
        int vendorID = mSharedPreferences.getInt("vendorID", -1);
        arrivedVendorImage = (RoundedImageView) dialog.findViewById(R.id.arrivedVendorImage);
        if (vendorID > 0) {
            Picasso.with(getActivity())
                    .load("http://www.WashMyWhip.us/wmwapp/VendorAvatarImages/vendor" + vendorID + "avatar.jpg")
                    .resize(100, 100)
                    .centerCrop()
                    .into(arrivedVendorImage);
        }

        dialog.show();
        return dialog;
    }

    private void initContact(View v) {
        textContact = (TextView) v.findViewById(R.id.contactText);
        textContact.setTypeface(mFont);
        textContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactTextVendor();
            }
        });
        callContact = (TextView) v.findViewById(R.id.contactCall);
        callContact.setTypeface(mFont);
        callContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactCallVendor();
            }
        });
        doneContact = (TextView) v.findViewById(R.id.contactDone);
        doneContact.setTypeface(mFont);
        doneContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void contactTextVendor() {
        String userNumber = mSharedPreferences.getString("vendorPhone", "null");
        if (!userNumber.equals("null")) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", userNumber, null)));
        }
    }

    public void contactCallVendor() {
        String userNumber = mSharedPreferences.getString("vendorPhone", "null");
        if (!userNumber.equals("null")) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + userNumber));
            startActivity(intent);
        }
    }
}


