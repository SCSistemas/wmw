package com.washmywhip.wmw;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.washmywhip.wmw.adapters.NavDrawerListAdapter;
import com.washmywhip.wmw.classes.NavDrawerItem;
import com.washmywhip.wmw.classes.Schedule;
import com.washmywhip.wmw.services.WashMyWhipEngine;
import com.washmywhip.wmw.util.RoundedImageView;
import com.washmywhip.wmw.util.ScrimInsetsFrameLayout;
import com.washmywhip.wmw.util.Util;
import com.washmywhip.wmw.util.WMW;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by Ross on 4/5/2016.
 */
public class WashMyWhipActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    private static final String TAG = "WashMyWhipActivity";
    private Toolbar toolbar;
    private ListView ndList;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ScrimInsetsFrameLayout sifl;
    private SharedPreferences mSharedPreferences;
    private Activity mActivity;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private WashMyWhipEngine mWashMyWhipEngine;
    private ProgressBar mProgressView;

    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private LatLng currentLocation;
    private LatLng cameraLocation;
    private Geocoder mGeocoder;
    private Marker mMarker;

    private ImageView setLocationButton;
    private TextView cancelButton;
    private TextView addressText;
    private String mAddress;
    private LinearLayout linearWashSchedule;
    private Button requestWashButton;
    private Button scheduleButton;
    private TextView confirmedAddress;
    private LinearLayout linearTypeWash;
    private TextView costWashPrice;
    private TextView costFullPrice;
    private TextView costExtraPrice;
    private TextView titleWashPrice;
    private TextView titleFullPrice;
    private TextView titleExtraPrice;

    private LinearLayout mLinearDescWashType;
    private TextView mTitleWashType;
    private TextView mDescWashType;
    private LinearLayout mLinearWash;
    private LinearLayout mLinearFull;
    private LinearLayout mLinearExtra;
    private String costSelected = "";
    private int washType;
    private String titleWashtype;
    private String costoWashType;
    private String costoWashTypeOne;
    private String costoWashTypeTwo;
    private String costoWashTypeThree;

    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;
    private RoundedImageView mImageProfile;
    private TextView mTextNameProfile;
    private LinearLayout mLinearProfile;
    private boolean callSchedule = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wash_my_wip);
        Fabric.with(this, new Crashlytics());
        mActivity = this;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mWashMyWhipEngine = new WashMyWhipEngine();
        sifl = (ScrimInsetsFrameLayout) findViewById(R.id.scrimInsetsFrameLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ndList = (ListView) findViewById(R.id.mListView);
        mGeocoder = new Geocoder(mActivity);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        View myLocationButton = mapFragment.getView().findViewById(0x2);

        if (myLocationButton != null && myLocationButton.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            // location button is inside of RelativeLayout
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) myLocationButton.getLayoutParams();

            // Align it to - parent BOTTOM|LEFT
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);

            final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80,
                    getResources().getDisplayMetrics());
            params.setMargins(margin, margin, margin, margin);

            myLocationButton.setLayoutParams(params);
        }
        mapFragment.getMapAsync(this);

        mProgressView = (ProgressBar) findViewById(R.id.progress_bar);

        setLocationButton = (ImageView) findViewById(R.id.setLocationButton);
        setLocationButton.setOnClickListener(this);

        cancelButton = (TextView) findViewById(R.id.cancelToolbarButton);
        cancelButton.setTypeface(mFont);
        cancelButton.setOnClickListener(this);

        linearWashSchedule = (LinearLayout) findViewById(R.id.linear_wash_schedule);
        linearWashSchedule.setOnClickListener(this);

        requestWashButton = (Button) findViewById(R.id.requestWashButton);
        requestWashButton.setTypeface(mFont);
        requestWashButton.setOnClickListener(this);

        scheduleButton = (Button) findViewById(R.id.scheduleButton);
        scheduleButton.setTypeface(mFont);
        scheduleButton.setOnClickListener(this);

        addressText = (EditText) findViewById(R.id.addressText);
        addressText.setTypeface(mFont);
        addressText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.d(TAG, "entered address");
                    setCameraToUserInput();
                }
                return false;
            }
        });

        confirmedAddress = (TextView) findViewById(R.id.confirmedAddress);
        confirmedAddress.setTypeface(mFont);

        linearTypeWash = (LinearLayout) findViewById(R.id.linear_type_wash);

        costWashPrice = (TextView) findViewById(R.id.costWashPrice);
        costFullPrice = (TextView) findViewById(R.id.costFullPrice);
        costExtraPrice = (TextView) findViewById(R.id.costExtraPrice);

        costWashPrice.setTypeface(mFontAvenir);
        costFullPrice.setTypeface(mFontAvenir);
        costExtraPrice.setTypeface(mFontAvenir);

        titleWashPrice = (TextView) findViewById(R.id.titleWashPrice);
        titleFullPrice = (TextView) findViewById(R.id.titleFullPrice);
        titleExtraPrice = (TextView) findViewById(R.id.titleExtraPrice);

        titleWashPrice.setTypeface(mFont);
        titleFullPrice.setTypeface(mFont);
        titleExtraPrice.setTypeface(mFont);

        findCostWash();
        findCostFull();
        findCostExtra();

        mLinearWash = (LinearLayout) findViewById(R.id.washPrice);
        mLinearFull = (LinearLayout) findViewById(R.id.fullPrice);
        mLinearExtra = (LinearLayout) findViewById(R.id.extraPrice);

        mLinearDescWashType = (LinearLayout) findViewById(R.id.linear_desc_type_wash);
        mTitleWashType = (TextView) findViewById(R.id.title_wash);
        mTitleWashType.setTypeface(mFont);
        mDescWashType = (TextView) findViewById(R.id.desc_wash);
        mDescWashType.setTypeface(mFontAvenir);

        mLinearWash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (costWashPrice.getText() != null) {
                    costSelected = costWashPrice.getText().toString();
                    mLinearWash.setBackgroundDrawable(getResources().getDrawable(R.drawable.rectangule_blue));
                    mLinearFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.rectangule));
                    mLinearExtra.setBackgroundDrawable(getResources().getDrawable(R.drawable.rectangule));
                    titleWashPrice.setTextColor(getResources().getColor(R.color.blue));
                    titleFullPrice.setTextColor(getResources().getColor(R.color.grey));
                    titleExtraPrice.setTextColor(getResources().getColor(R.color.grey));
                    mLinearDescWashType.setVisibility(View.VISIBLE);
                    mTitleWashType.setText(getResources().getString(R.string.type_wash_one) + ":");
                    mDescWashType.setText(getResources().getString(R.string.desc_type_wash_one));
                    titleWashtype = getResources().getString(R.string.type_wash_one);
                    costoWashType = costoWashTypeOne;
                    washType = 0;
                }
            }
        });

        mLinearFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (costFullPrice.getText() != null) {
                    costSelected = costFullPrice.getText().toString();
                    mLinearWash.setBackgroundDrawable(getResources().getDrawable(R.drawable.rectangule));
                    mLinearFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.rectangule_blue));
                    mLinearExtra.setBackgroundDrawable(getResources().getDrawable(R.drawable.rectangule));
                    titleWashPrice.setTextColor(getResources().getColor(R.color.grey));
                    titleFullPrice.setTextColor(getResources().getColor(R.color.blue));
                    titleExtraPrice.setTextColor(getResources().getColor(R.color.grey));
                    mLinearDescWashType.setVisibility(View.VISIBLE);
                    mTitleWashType.setText(getResources().getString(R.string.type_wash_two) + ":");
                    mDescWashType.setText(getResources().getString(R.string.desc_type_wash_two));
                    titleWashtype = getResources().getString(R.string.type_wash_two);
                    costoWashType = costoWashTypeTwo;
                    washType = 1;
                }
            }
        });

        mLinearExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (costExtraPrice.getText() != null) {
                    costSelected = costExtraPrice.getText().toString();
                    mLinearWash.setBackgroundDrawable(getResources().getDrawable(R.drawable.rectangule));
                    mLinearFull.setBackgroundDrawable(getResources().getDrawable(R.drawable.rectangule));
                    mLinearExtra.setBackgroundDrawable(getResources().getDrawable(R.drawable.rectangule_blue));
                    titleWashPrice.setTextColor(getResources().getColor(R.color.grey));
                    titleFullPrice.setTextColor(getResources().getColor(R.color.grey));
                    titleExtraPrice.setTextColor(getResources().getColor(R.color.blue));
                    mLinearDescWashType.setVisibility(View.VISIBLE);
                    mTitleWashType.setText(getResources().getString(R.string.type_wash_three) + ":");
                    mDescWashType.setText(getResources().getString(R.string.desc_type_wash_three));
                    titleWashtype = getResources().getString(R.string.type_wash_three);
                    costoWashType = costoWashTypeThree;
                    washType = 2;
                }
            }
        });

        mImageProfile = (RoundedImageView) findViewById(R.id.profilePicture);
        mTextNameProfile = (TextView) findViewById(R.id.nameProfile);
        mLinearProfile = (LinearLayout) findViewById(R.id.linearProfile);
        mLinearProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WashMyWhipActivity.this, ProfileActivity.class));
                drawerLayout.closeDrawer(sifl);
            }
        });

        String name = mSharedPreferences.getString("FirstName", "");
        int userid = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        if (userid >= 0) {
            Picasso.with(mActivity)
                    .load(mActivity.getResources().getString(R.string.url_app) + "/ClientAvatarImages/client" + userid + "avatar.jpg")
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .resize(200, 200)
                    .centerCrop().placeholder(R.drawable.picture).error(R.drawable.picture).into(mImageProfile);
        }
        if (name.isEmpty()) {
            name = "WMW";
        }
        mTextNameProfile.setTypeface(mFont);
        mTextNameProfile.setText(name.toUpperCase());

        TextView seeMyProfile = (TextView) findViewById(R.id.see_my_profile);
        seeMyProfile.setTypeface(mFont);

        ArrayList<NavDrawerItem> data = new ArrayList<>();
        data.add(new NavDrawerItem("PAYMENT", R.drawable.paymenticon));
        data.add(new NavDrawerItem("MY WASHES", R.drawable.schedule));
        data.add(new NavDrawerItem("FEEDBACK", R.drawable.feedback));
        data.add(new NavDrawerItem("PROMOTIONS", R.drawable.promo_code));
        data.add(new NavDrawerItem("ABOUT", R.drawable.abouticon));

        ndList.setAdapter(new NavDrawerListAdapter(this, R.layout.nav_drawer_item, data));
        ndList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                if (pos == 0) {
                    startActivity(new Intent(WashMyWhipActivity.this, PaymentActivity.class));
                } else if (pos == 1) {
                    startActivity(new Intent(WashMyWhipActivity.this, ListScheduleActivity.class));
                } else if (pos == 2) {
                    startActivity(new Intent(WashMyWhipActivity.this, FeedbackActivity.class));
                } else if (pos == 3) {
                    startActivity(new Intent(WashMyWhipActivity.this, PromoCodeActivity.class));
                } else if (pos == 4) {
                    startActivity(new Intent(WashMyWhipActivity.this, AboutActivity.class));
                }
                drawerLayout.closeDrawer(sifl);
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.hamburger, R.string.drawerOpen, R.string.drawerClose) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.hamburger);
        showAlertDialog();
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();
        if (id == R.id.home) {
        }

        return super.onOptionsItemSelected(item);
    }

    public void setCameraToUserInput() {
        List<Address> addresses = null;
        try {
            addresses = mGeocoder.getFromLocationName(addressText.getText().toString(), 5);
        } catch (IOException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
        if (addresses.size() > 0) {
            currentLocation = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.0f));
            addressText.setText(addresses.get(0).getAddressLine(0));
        } else {
            Util.showAlertDialogDismiss(WashMyWhipActivity.this, "Invalid Location", "Please Provide the Proper Place");
        }
    }


    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            if (currentLocation != null && mMap != null) {
                //Only force a camera update if user has traveled 0.1 miles from last location
                if (Util.distance(currentLocation.latitude, currentLocation.longitude, loc.latitude, loc.longitude) > 0.1) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
                    currentLocation = loc;
                }
            } else {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
            }
            currentLocation = loc;
            mMap.setOnMyLocationChangeListener(null);
        }
    };

    public JSONObject getLocationInfo(double lat, double lng) {
        HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=true");
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            stream.close();
        } catch (ClientProtocolException e) {
            Crashlytics.logException(e);
        } catch (IOException e) {
            Crashlytics.logException(e);
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
        return jsonObject;
    }

    private GoogleMap.OnCameraChangeListener myCameraChangeListener = new GoogleMap.OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition cameraPosition) {
            cameraLocation = cameraPosition.target;
            JSONObject ret = getLocationInfo(cameraLocation.latitude, cameraLocation.longitude);
            currentLocation = new LatLng(cameraLocation.latitude, cameraLocation.longitude);
            JSONObject location;
            String location_string;
            try {
                if (ret.getJSONArray("results") != null) {
                    JSONArray results = ret.getJSONArray("results");
                    if (results.length() > 0) {
                        if (ret.getJSONArray("results").getJSONObject(0) != null) {
                            location = ret.getJSONArray("results").getJSONObject(0);
                            location_string = location.getString("formatted_address");
                            Log.d("test", "formattted address:" + location_string);
                            mAddress = location_string;
                            addressText.setText(mAddress);
                            if (addressText.hasFocus()) {
                                Util.hideKeyboard(addressText, mActivity);
                            }
                        }
                    }
                }
            } catch (JSONException e1) {
                Crashlytics.logException(e1);
                e1.printStackTrace();
            }
        }
    };

    private GoogleMap.OnMapClickListener myMapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
            Log.d(TAG, "clearing focus on address text");
            if (addressText != null && addressText.hasFocus()) {
                Log.d(TAG, "address text has focus");
                Util.hideKeyboard(addressText, mActivity);
            }
        }
    };

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.requestWashButton) {
            requestWashButton();
        } else {
            if (v.getId() == R.id.scheduleButton) {
                getCars("schedule");
            }
        }
    }

    public void requestWashButton() {
        if (!costSelected.equalsIgnoreCase("")) {
            getCars("wash_now");
        } else {
            Toast.makeText(mActivity, R.string.choose_wash, Toast.LENGTH_SHORT).show();
        }
    }

    public void getCars(final String parent) {
        showProgress(true);
        int userIDnum = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        if (userIDnum >= 0) {
            mWashMyWhipEngine.getCars(userIDnum, new Callback<List<JSONObject>>() {
                @Override
                public void success(List<JSONObject> jsonObject, Response response) {
                    String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                    Log.d("getCars", " response: " + responseString);
                    if (responseString.equals("[]")) {
                        Log.d("getCars", " response is null ");
                        Util.showAlertDialogDismiss(WashMyWhipActivity.this, getResources().getString(R.string.any_car),
                                getResources().getString(R.string.add_car_before_request_wash));
                        showProgress(false);
                    } else {
                        getCards(parent);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("getCars", "error: " + error.toString());
                    showProgress(false);
                }
            });
        }
    }

    public void getCards(final String parent) {
        showProgress(true);
        int userID = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        if (userID >= 0) {
            mWashMyWhipEngine.getStripeCustomer(userID, new Callback<JSONObject>() {
                @Override
                public void success(JSONObject jsonObject, Response response) {
                    String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                    JSONObject json = null;
                    if (!responseString.equalsIgnoreCase("No Stripe Customer")) {
                        try {
                            json = new JSONObject(responseString);
                            Map<String, Object> paymentData = Util.jsonToMap(json);
                            Log.d("getCards", "" + json.toString());
                            Log.d("getCards", "default source: " + json.getString("default_source"));
                            String defaultCard = json.getString("default_source");
                            mSharedPreferences.edit().putString("default_source", defaultCard).apply();
                            if (defaultCard != null && defaultCard.length() > 5) {
                                if (parent.equalsIgnoreCase("wash_now")) {
                                    callSchedule();
                                } else {
                                    startActivity(new Intent(WashMyWhipActivity.this, CalendarActivity.class));
                                }
                            } else {
                                Util.showAlertDialogDismiss(WashMyWhipActivity.this, getResources().getString(R.string.any_default_card),
                                        getResources().getString(R.string.make_payment_default_before_request_was));
                            }
                            showProgress(false);
                        } catch (JSONException e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                            showProgress(false);
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("getCards", "failz: " + error.getMessage());
                    Util.showAlertDialogDismiss(WashMyWhipActivity.this, getResources().getString(R.string.any_card), getResources().getString(R.string.add_payment_before_request_wash));
                    showProgress(false);
                }
            });
        }
    }

    public void showAlertDialog() {
        dialogBuilder = new AlertDialog.Builder(WashMyWhipActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_welcome, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        TextView mSuccess = (TextView) dialogView.findViewById(R.id.welcome_success);
        mSuccess.setTypeface(mFont);
        TextView mWelcome = (TextView) dialogView.findViewById(R.id.welcome_welcome);
        mWelcome.setTypeface(mFontAvenir);
        TextView mThanks = (TextView) dialogView.findViewById(R.id.welcome_thanks);
        mThanks.setTypeface(mFontAvenir);
        LinearLayout linearWelcome = (LinearLayout) dialogView.findViewById(R.id.linear_dialog_welcome);
        linearWelcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    public void findCostWash() {
        showProgress(true);
        final String body = "typeVal=0";
        RequestQueue queue = Volley.newRequestQueue(WashMyWhipActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        if (!response.contains(getResources().getString(R.string.error))) {
                            costWashPrice.setText("$" + response);
                            costoWashTypeOne = response;
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(WashMyWhipActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    public void findCostFull() {
        showProgress(true);
        final String body = "typeVal=1";
        RequestQueue queue = Volley.newRequestQueue(WashMyWhipActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        if (!response.contains(getResources().getString(R.string.error))) {
                            costFullPrice.setText("$" + response);
                            costoWashTypeTwo = response;
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    public void findCostExtra() {
        showProgress(true);
        final String body = "typeVal=2";
        RequestQueue queue = Volley.newRequestQueue(WashMyWhipActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        if (!response.contains(getResources().getString(R.string.error))) {
                            costExtraPrice.setText("$" + response);
                            costoWashTypeThree = response;
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.d("TEST", "map ready");
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationChangeListener(myLocationChangeListener);
        mMap.setOnCameraChangeListener(myCameraChangeListener);
        mMap.setOnMapClickListener(myMapClickListener);
    }

    public void callSchedule() {
        Intent i = new Intent(WashMyWhipActivity.this, ScheduleActivity.class);
        setDate();
        setLocation();
        WMW.getSchedule().setWashType(washType);
        i.putExtra("parent", "wash_now");
        i.putExtra("start_time", validTimeBegin());
        i.putExtra("end_time", validTimeEnd());
        i.putExtra("title_wash_type", titleWashtype);
        i.putExtra("costo_wash_type", costoWashType);
        if (callSchedule) {
            startActivity(i);
        }
    }


    public void setDate() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        String date = df.format(new java.util.Date());
        Schedule schedule = new Schedule();
        schedule.setDate(date);
        WMW.setSchedule(schedule);
    }

    public void setLocation() {
        if (!addressText.getText().toString().isEmpty()) {
            mMarker = mMap.addMarker(new MarkerOptions().position(cameraLocation).visible(false));
            if (currentLocation != null) {
                WMW.getSchedule().setLat(currentLocation.latitude);
                WMW.getSchedule().setLng(currentLocation.longitude);
            }
            String address[] = addressText.getText().toString().split(",");
            if (address[0] != null) {
                WMW.getSchedule().setLocationName(address[0].toString());
                WMW.getSchedule().setLocationStreet(address[0].toString());
            }
            if (address[1] != null) {
                WMW.getSchedule().setLocationCity(address[1].toString());
            }
            if (address.length > 2) {
                if (address[2] != null) {
                    WMW.getSchedule().setLocationState(address[2].toString());
                }
            }

            if (address.length > 3) {
                if (address[3] != null) {
                    WMW.getSchedule().setLocationCountry(address[3].toString());
                }
            }
        }
    }

    public String validTimeBegin() {
        callSchedule = true;
        String state = "";
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        calendar.get(Calendar.HOUR);
        if (calendar.get(Calendar.AM_PM) == 0)
            state = " AM";
        else
            state = " PM";

        if (calendar.get(Calendar.HOUR) <= 7 && state.contains("AM")) {
            calendar.set(Calendar.HOUR, 9);
            calendar.set(Calendar.MINUTE, 00);
        } else {
            if (calendar.get(Calendar.HOUR) >= 7 && state.contains("AM") || calendar.get(Calendar.HOUR) <= 7 && state.contains("PM")) {
                calendar.add(Calendar.HOUR, 1);
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        String time = sdf.format(calendar.getTime());
        String startTime = WMW.getSchedule().getDate() + " " + time + " " + state;
        WMW.getSchedule().setStartTime(startTime);
        return time + " " + state;
    }

    public String validTimeEnd() {
        String state = "";
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        calendar.get(Calendar.HOUR);
        if (calendar.get(Calendar.AM_PM) == 0)
            state = " AM";
        else
            state = " PM";

        System.out.println(calendar.get(Calendar.HOUR) + " " + state);
        if (((calendar.get(Calendar.HOUR) <= 12 && state.contains("AM")) && (calendar.get(Calendar.HOUR) >= 07 && state.contains("AM"))) ||
                (calendar.get(Calendar.HOUR) <= 3 && state.contains("PM"))) {
            calendar.set(Calendar.HOUR, 5);
            calendar.set(Calendar.MINUTE, 00);
            state = " PM";
        } else {
            if (calendar.get(Calendar.HOUR) >= 3 && state.contains("PM") || calendar.get(Calendar.HOUR) <= 7 && state.contains("PM")) {
                calendar.set(Calendar.HOUR, 9);
                calendar.set(Calendar.MINUTE, 00);
                state = " PM";
            } else {
                if (calendar.get(Calendar.HOUR) >= 7 && state.contains("PM")) {
                    callSchedule = false;
                    Util.showAlertDialogDismiss(mActivity, getResources().getString(R.string.too_late),
                            getResources().getString(R.string.valid_time_schedule));
                }
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        String time = sdf.format(calendar.getTime());
        String endTime = WMW.getSchedule().getDate() + " " + time + " " + state;
        WMW.getSchedule().setEndTime(endTime);
        return time + " " + state;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }
}


