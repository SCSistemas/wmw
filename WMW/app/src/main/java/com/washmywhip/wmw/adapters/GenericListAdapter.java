package com.washmywhip.wmw.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.washmywhip.wmw.R;
import com.washmywhip.wmw.classes.Generic;

import java.util.ArrayList;


public class GenericListAdapter extends ArrayAdapter<Generic> {
    private Context context;
    private ArrayList<Generic> washItems;
    private int resourceID;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private SharedPreferences mSharedPreferences;


    public GenericListAdapter(Context context, int resource, ArrayList<Generic> washItems) {
        super(context, resource, washItems);
        mFont = Typeface.createFromAsset(context.getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        this.washItems = washItems;
        this.context = context;
        this.resourceID = resource;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public int getCount() {
        return washItems.size();
    }

    @Override
    public Generic getItem(int position) {
        return washItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();
            view = inflater.inflate(resourceID, parent, false);
            drawerHolder.left = (TextView) view.findViewById(R.id.genericLeft);
            drawerHolder.right = (TextView) view.findViewById(R.id.genericRight);
            view.setTag(drawerHolder);
        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();
        }

        Generic item = this.washItems.get(position);
        drawerHolder.left.setText(item.getLeft());
        drawerHolder.left.setTypeface(mFont);
        drawerHolder.right.setText(item.getRight());
        drawerHolder.right.setTypeface(mFont);

        return view;
    }

    private static class DrawerItemHolder {
        TextView left;
        TextView right;

    }
}
