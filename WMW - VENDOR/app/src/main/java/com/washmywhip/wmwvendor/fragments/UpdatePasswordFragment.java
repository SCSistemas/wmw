package com.washmywhip.wmwvendor.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.washmywhip.wmwvendor.R;

/**
 * Created by enny.querales on 13/6/2016.
 */
public class UpdatePasswordFragment extends DialogFragment {

    private EditText mEditUpdatePass;
    private Button mButtonUpdatePass;
    private Button mButtonCancelPass;
    private Typeface mFont;
    private Typeface mFontAvenir;


    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface UpdatePasswordFragmentDialogListener {
        public void onDialogUpdatePassword(String password);
    }

    private UpdatePasswordFragmentDialogListener mListener;

    public static UpdatePasswordFragment newInstance(UpdatePasswordFragmentDialogListener mListener) {

        UpdatePasswordFragment fragment = new UpdatePasswordFragment();
        fragment.mListener = mListener;
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_fragment_update_password);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Avenir.ttc");
        wlp.gravity = Gravity.CENTER;
        //  wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

        mEditUpdatePass = (EditText) dialog.findViewById(R.id.update_password_text);
        mEditUpdatePass.setTypeface(mFontAvenir);
        mButtonUpdatePass = (Button) dialog.findViewById(R.id.update_password_submit);
        mButtonCancelPass = (Button) dialog.findViewById(R.id.update_password_cancel);
        mButtonUpdatePass.setTypeface(mFont);
        mButtonCancelPass.setTypeface(mFont);

        mButtonUpdatePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mEditUpdatePass.getText().toString().isEmpty()) {
                    mListener.onDialogUpdatePassword(mEditUpdatePass.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        mButtonCancelPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        return dialog;
    }
}

