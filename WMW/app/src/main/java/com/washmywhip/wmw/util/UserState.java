package com.washmywhip.wmw.util;

/**
 * Created by Ross on 2/1/2016.
 */
public enum UserState {
    REQUESTING,CONFIRMING,QUEUED,WAITING,ARRIVED, WASHING,FINALIZING
}
