package com.washmywhip.wmw;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class LauncherActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean value = prefs.getBoolean("isLoggedIn", false);
        System.out.println("value: " + prefs.getBoolean("isLoggedIn", false));
        if (!value) {
            //Login activity
            Intent i = new Intent(this, WelcomeActivity.class);
            startActivity(i);
            finish();
        } else {
            //Main Activity
            Intent i = new Intent(this, WashMyWhipActivity.class);
            startActivity(i);
            finish();
        }
    }
}
