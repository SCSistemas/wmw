package com.washmywhip.wmwvendor;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    EditText email;
    Button submit;
    private View mProgressView;
    private TextView forgotPassword;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private TextView mDescForgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_layout);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        forgotPassword = (TextView) findViewById(R.id.forgot_your_password);
        forgotPassword.setTypeface(mFont);
        email = (EditText) findViewById(R.id.forgotPasswordEmail);
        email.setTypeface(mFontAvenir);
        submit = (Button) findViewById(R.id.submitForgotPassswordButton);
        submit.setOnClickListener(this);
        mDescForgot = (TextView) findViewById(R.id.desc_forgot);
        mDescForgot.setTypeface(mFontAvenir);
        submit.setTypeface(mFont);
        mProgressView = findViewById(R.id.progress_bar);
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void requestTemporaryPasswordWithVolley() {
        showProgress(true);
        final String body = "email=" + email.getText().toString();
        RequestQueue queue = Volley.newRequestQueue(ForgotPasswordActivity.this);
        String url = getResources().getString(R.string.url_app) + "/requestTemporaryPasswordForVendor.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        System.out.println("respuesta: " + response);
                        if (!response.contains("1")) {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
                        } else if (response.contains("1")) {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.review_email_new_pawword, Toast.LENGTH_LONG).show();
                            Intent i = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                            startActivity(i);
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ForgotPasswordActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }
        };
        queue.add(sr);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == submit.getId()) {
            if (!email.getText().toString().isEmpty()) {
                requestTemporaryPasswordWithVolley();
            }
        }
    }
}
