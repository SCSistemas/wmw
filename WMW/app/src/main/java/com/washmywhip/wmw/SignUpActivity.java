package com.washmywhip.wmw;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.washmywhip.wmw.util.Util;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    private Button singup;
    private EditText usernameEdit;
    private EditText passwordEdit;
    private EditText reenterPasswordEdit;
    private EditText emailEdit;
    private EditText phoneEdit;
    private EditText firstNameEdit;
    private EditText lastNameEdit;
    private LinearLayout mView;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private EditText[] fields;
    private View mProgressView;
    private ImageView back;
    private TextView pageWMW;
    TextView.OnEditorActionListener mOnEditActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                hideKeyboard(v);
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        usernameEdit = (EditText) findViewById(R.id.usernameSignup);
        usernameEdit.setTypeface(mFontAvenir);
        passwordEdit = (EditText) findViewById(R.id.passwordSignup);
        passwordEdit.setTypeface(mFontAvenir);
        reenterPasswordEdit = (EditText) findViewById(R.id.reenterPasswordSignup);
        reenterPasswordEdit.setTypeface(mFontAvenir);
        phoneEdit = (EditText) findViewById(R.id.phoneSignup);
        phoneEdit.setTypeface(mFontAvenir);
        emailEdit = (EditText) findViewById(R.id.emailSignup);
        emailEdit.setTypeface(mFontAvenir);
        firstNameEdit = (EditText) findViewById(R.id.firstNameSignup);
        firstNameEdit.setTypeface(mFontAvenir);
        lastNameEdit = (EditText) findViewById(R.id.lastNameSignup);
        lastNameEdit.setTypeface(mFontAvenir);
        back = (ImageView) findViewById(R.id.signup_back);
        mView = (LinearLayout) findViewById(R.id.signUpLayout);
        pageWMW = (TextView) findViewById(R.id.pagewmw);
        pageWMW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("http://www.washmywhip.com/legal"));
                    startActivity(i);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(SignUpActivity.this, "No application can handle this request.", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
        fields = new EditText[]{usernameEdit, passwordEdit, reenterPasswordEdit, emailEdit, phoneEdit, firstNameEdit, lastNameEdit};
        for (EditText field : fields) {
            field.setOnEditorActionListener(mOnEditActionListener);
        }
        singup = (Button) findViewById(R.id.signupSignup);
        singup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRegister();
            }
        });
        singup.setTypeface(mFont);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mProgressView = findViewById(R.id.progress_bar);
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void attemptRegister() {
        // Reset errors.
        usernameEdit.setError(null);
        passwordEdit.setError(null);
        reenterPasswordEdit.setError(null);
        emailEdit.setError(null);
        phoneEdit.setError(null);
        firstNameEdit.setError(null);
        lastNameEdit.setError(null);

        // Store values at the time of the login attempt.
        String username = usernameEdit.getText().toString();

        String email = emailEdit.getText().toString();
        String password = passwordEdit.getText().toString();
        String reenterPassword = reenterPasswordEdit.getText().toString();
        String phone = phoneEdit.getText().toString();
        String firstName = firstNameEdit.getText().toString();
        String lastName = lastNameEdit.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(lastName)) {
            lastNameEdit.setError(getString(R.string.error_field_required));
            focusView = lastNameEdit;
            cancel = true;
        }

        if (TextUtils.isEmpty(firstName)) {
            firstNameEdit.setError(getString(R.string.error_field_required));
            focusView = firstNameEdit;
            cancel = true;
        }

        if (TextUtils.isEmpty(phone)) {
            phoneEdit.setError(getString(R.string.error_field_required));
            focusView = phoneEdit;
            cancel = true;
        }

        if (!TextUtils.isEmpty(reenterPasswordEdit.getText().toString()) && !TextUtils.isEmpty(passwordEdit.getText().toString())) {
            if (!reenterPasswordEdit.getText().toString().equals(passwordEdit.getText().toString())) {
                reenterPasswordEdit.setError(getString(R.string.password_not_math));
                focusView = reenterPasswordEdit;
                cancel = true;
            }
        }

        if (!TextUtils.isEmpty(reenterPasswordEdit.getText().toString()) && !Util.isPasswordValid(reenterPasswordEdit.getText().toString())) {
            reenterPasswordEdit.setError(getString(R.string.error_invalid_password));
            focusView = reenterPasswordEdit;
            cancel = true;
        }


        if (!TextUtils.isEmpty(passwordEdit.getText().toString()) && !Util.isPasswordValid(passwordEdit.getText().toString())) {
            passwordEdit.setError(getString(R.string.error_invalid_password));
            focusView = passwordEdit;
            cancel = true;
        }

        if (TextUtils.isEmpty(reenterPassword)) {
            reenterPasswordEdit.setError(getString(R.string.error_field_required));
            focusView = reenterPasswordEdit;
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            passwordEdit.setError(getString(R.string.error_field_required));
            focusView = passwordEdit;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            emailEdit.setError(getString(R.string.error_field_required));
            focusView = emailEdit;
            cancel = true;
        } else if (!Util.isEmailValid(emailEdit.getText().toString())) {
            emailEdit.setError(getString(R.string.error_invalid_email));
            focusView = emailEdit;
            cancel = true;
        }

        if (TextUtils.isEmpty(username)) {
            usernameEdit.setError(getString(R.string.error_field_required));
            focusView = usernameEdit;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            signUpWithVolley();
        }
    }

    public void signUpWithVolley() {
        showProgress(true);
        final String body = "username=" + usernameEdit.getText().toString() +
                "&password=" + passwordEdit.getText().toString() +
                "&email=" + emailEdit.getText().toString() +
                "&firstname=" + firstNameEdit.getText().toString() +
                "&lastname=" + lastNameEdit.getText().toString() +
                "&phone=" + phoneEdit.getText().toString();
        RequestQueue queue = Volley.newRequestQueue(SignUpActivity.this);
        String url = getResources().getString(R.string.url_app) + "/createUser.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        System.out.println("respuesta: " + response);
                        if (response.contains("Exists")) {
                            Toast.makeText(SignUpActivity.this, response, Toast.LENGTH_LONG).show();
                        } else {
                            if (response.equalsIgnoreCase(getResources().getString(R.string.error_try_again))) {
                                Toast.makeText(SignUpActivity.this, response, Toast.LENGTH_LONG).show();
                            } else {
                                if (response.equalsIgnoreCase("1")) {
                                    Toast.makeText(SignUpActivity.this, R.string.info_save, Toast.LENGTH_LONG).show();
                                    Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Toast.makeText(SignUpActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                Toast.makeText(SignUpActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }
        };
        queue.add(sr);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == mView.getId()) {
            for (EditText field : fields) {
                if (field.hasFocus()) {
                    hideKeyboard(field);
                }
            }
        }
        return true;
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }
}
