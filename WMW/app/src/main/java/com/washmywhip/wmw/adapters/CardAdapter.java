package com.washmywhip.wmw.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.washmywhip.wmw.R;
import com.washmywhip.wmw.classes.Card;

import java.util.List;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {

    private Activity mActivity;
    private List<Card> result;
    private static ClickListener clickListener;
    private Typeface mFont;
    private Typeface mFontAvenir;

    public CardAdapter(Activity context, List<Card> result) {
        this.result = result;
        this.mActivity = context;
        mFont = Typeface.createFromAsset(context.getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
    }

    @Override
    public CardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(final CardAdapter.ViewHolder holder, final int position) {
        final Card card = result.get(position);
        String CCText = "**** " + card.getLastFour();
        holder.cardNumber.setText(CCText);
        holder.cardNumber.setTypeface(mFontAvenir);
        if (card.isActive()) {
            holder.active.setText(R.string.active_card);
            holder.active.setTypeface(mFont);
        } else {
            holder.active.setVisibility(View.INVISIBLE);
        }

        if (card.getCardType().contains("Visa")) {
            Picasso.with(mActivity)
                    .load(R.drawable.visa)
                    .resize(100, 100)
                    .centerCrop()
                    .into(holder.cardBrand);
        } else {
            if (card.getCardType().contains("Master")) {
                Picasso.with(mActivity)
                        .load(R.drawable.master_card)
                        .resize(100, 100)
                        .centerCrop()
                        .into(holder.cardBrand);
            } else {
                if (card.getCardType().contains("America")) {
                    Picasso.with(mActivity)
                            .load(R.drawable.amex)
                            .resize(100, 100)
                            .centerCrop()
                            .into(holder.cardBrand);
                } else {
                    if (card.getCardType().contains("Dine")) {
                        Picasso.with(mActivity)
                                .load(R.drawable.diners_club)
                                .resize(100, 100)
                                .centerCrop()
                                .into(holder.cardBrand);
                    } else {
                        if (card.getCardType().contains("Disco")) {
                            Picasso.with(mActivity)
                                    .load(R.drawable.discover)
                                    .resize(100, 100)
                                    .centerCrop()
                                    .into(holder.cardBrand);
                        }
                    }
                }
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView cardBrand;
        public TextView cardNumber;
        public TextView active;

        public ViewHolder(View mView) {
            super(mView);
            mView.setOnClickListener(this);
            cardBrand = (ImageView) mView.findViewById(R.id.card_image);
            cardNumber = (TextView) mView.findViewById(R.id.cardNumber);
            active = (TextView) mView.findViewById(R.id.cardActive);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        CardAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    @Override
    public int getItemCount() {
        return result.size();
    }

    public void add(Card card) {
        result.add(card);
        notifyItemInserted(result.size() - 1);
    }

    public void remove(int position) {
        result.remove(position);
        notifyItemRemoved(position);
    }

    public Card getCard(int position) {
        return result.get(position);
    }

    public Card[] getCards() {
        Card[] cardArray = new Card[result.size()];
        return result.toArray(cardArray);
    }
}
