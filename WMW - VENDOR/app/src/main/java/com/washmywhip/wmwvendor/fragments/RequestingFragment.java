package com.washmywhip.wmwvendor.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.washmywhip.wmwvendor.R;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class RequestingFragment extends DialogFragment {
    private Button acceptRequest;
    private CountDownTimer mCountDownTimer;
    private Typeface mFont;
    private TextView userFullName;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface RequestingFragmentDialogListener {
        public void onDialogRequesting(boolean expires);
    }

    private RequestingFragmentDialogListener mListener;

    public static RequestingFragment newInstance(RequestingFragmentDialogListener mListener) {

        RequestingFragment fragment = new RequestingFragment();
        fragment.mListener = mListener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_init_requesting);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Archive.otf");
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        acceptRequest = (Button) dialog.findViewById(R.id.acceptRequest);
        acceptRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCountDownTimer != null) {
                    mCountDownTimer.cancel();
                }
                mListener.onDialogRequesting(false);
                dialog.dismiss();
            }
        });
        acceptRequest.setTypeface(mFont);
        userFullName = (TextView) dialog.findViewById(R.id.requestingUserName);

        final TextView timer = (TextView) dialog.findViewById(R.id.progressText);
        mCountDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timer.setText("" + millisUntilFinished / 1000);
            }
            @Override
            public void onFinish() {
                mListener.onDialogRequesting(true);
                dialog.dismiss();
            }
        };
        mCountDownTimer.start();
        dialog.show();
        return dialog;
    }
}


