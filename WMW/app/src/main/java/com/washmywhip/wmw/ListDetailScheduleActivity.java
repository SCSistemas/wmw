package com.washmywhip.wmw;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.washmywhip.wmw.classes.Car;
import com.washmywhip.wmw.classes.Schedule;
import com.washmywhip.wmw.fragments.FinalizingFragment;
import com.washmywhip.wmw.services.WashMyWhipEngine;
import com.washmywhip.wmw.util.RoundedImageView;
import com.washmywhip.wmw.util.Util;

public class ListDetailScheduleActivity extends AppCompatActivity implements View.OnClickListener, FinalizingFragment.FinalizingFragmentDialogListener {
    private Button rateWash;
    private SharedPreferences mSharedPreferences;
    private WashMyWhipEngine mWashMyWhipEngine;
    private int userID;
    ImageView onBack;
    private View mProgressView;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private TextView mTextDate;
    private TextView mTextDesc;
    private RoundedImageView mCarImage;
    private TextView mCarColor;
    private TextView mCarModel;
    private TextView mCarPlate;
    private String washName;
    private Schedule schedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_detail_schedule);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ListDetailScheduleActivity.this);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        userID = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        mWashMyWhipEngine = new WashMyWhipEngine();
        mProgressView = findViewById(R.id.progress_bar);
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rateWash = (Button) findViewById(R.id.rateWash);
        rateWash.setOnClickListener(this);
        rateWash.setTypeface(mFont);
        mTextDate = (TextView) findViewById(R.id.detailScheduleDate);
        mTextDesc = (TextView) findViewById(R.id.detailScheduleDesc);
        mCarImage = (RoundedImageView) findViewById(R.id.carPictureProfile);
        mCarColor = (TextView) findViewById(R.id.carColorProfile);
        mCarModel = (TextView) findViewById(R.id.carModelProfile);
        mCarPlate = (TextView) findViewById(R.id.carePlateProfile);

        Intent i = getIntent();
        schedule = i.getParcelableExtra("schedule");
        Car car = schedule.getCar();

        if (schedule.getWashType() == 0) {
            findCostWash();
        } else {
            if (schedule.getWashType() == 1) {
                findCostFull();
            } else {
                if (schedule.getWashType() == 2) {
                    findCostExtra();
                }
            }
        }

        mTextDate.setText(schedule.getDate());
        mTextDate.setTypeface(mFont);
        mTextDesc.setTypeface(mFontAvenir);
        Picasso.with(ListDetailScheduleActivity.this)
                .load(ListDetailScheduleActivity.this.getResources().getString(R.string.url_app) + "/CarImages/car" + car.getCarID() + "image.jpg")
                .resize(100, 100)
                .centerCrop()
                .into(mCarImage);

        mCarColor.setText(car.getColor());
        mCarColor.setTypeface(mFont);
        mCarModel.setText(car.getMake() + " " + car.getModel());
        mCarModel.setTypeface(mFont);
        mCarPlate.setText(car.getPlate());
        mCarPlate.setTypeface(mFont);
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void findCostWash() {
        showProgress(true);
        final String body = "typeVal=0";
        RequestQueue queue = Volley.newRequestQueue(ListDetailScheduleActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        if (!response.contains(getResources().getString(R.string.error))) {
                            washName = "Fresh";
                            mTextDesc.setText("A " + washName + " Wash for $" + schedule.getPrice());
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ListDetailScheduleActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    public void findCostFull() {
        showProgress(true);
        final String body = "typeVal=1";
        RequestQueue queue = Volley.newRequestQueue(ListDetailScheduleActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        if (!response.contains(getResources().getString(R.string.error))) {
                            washName = "Luxe";
                            mTextDesc.setText("A " + washName + " Wash for $" + schedule.getPrice());
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    public void findCostExtra() {
        showProgress(true);
        final String body = "typeVal=2";
        RequestQueue queue = Volley.newRequestQueue(ListDetailScheduleActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        if (!response.contains(getResources().getString(R.string.error))) {
                            washName = "Royal";
                            mTextDesc.setText("A " + washName + " Wash for $" + schedule.getPrice());
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == rateWash.getId()) {
            if (schedule.getCompleted() != 0) {
                FinalizingFragment fragment = FinalizingFragment.newInstance(ListDetailScheduleActivity.this);
                Bundle bundle = new Bundle();
                bundle.putParcelable("schedule", schedule);
                fragment.setArguments(bundle);
                fragment.show(getFragmentManager(), "finalizing");
            } else {
                Util.showAlertDialogDismiss(ListDetailScheduleActivity.this, "Oops!", "This washing has not been completed");
            }
        }
    }

    @Override
    public void onDialogFinalizing(int rating, String comments) {

    }
}
