package com.washmywhip.wmw;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.washmywhip.wmw.classes.PromoCode;
import com.washmywhip.wmw.util.Util;

public class PromoCodeActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView promoCode;
    private EditText mPromoCode;
    private Button submit;
    private View mProgressView;
    private SharedPreferences mSharedPreferences;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private TextView mDescPromo;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo_code);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        promoCode = (TextView) findViewById(R.id.promoCode);
        promoCode.setTypeface(mFont);
        mPromoCode = (EditText) findViewById(R.id.promoCodeInfo);
        mPromoCode.setTypeface(mFontAvenir);
        submit = (Button) findViewById(R.id.promoCodeButton);
        submit.setOnClickListener(this);
        submit.setTypeface(mFont);
        mDescPromo = (TextView) findViewById(R.id.desc_promo);
        mDescPromo.setTypeface(mFontAvenir);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mProgressView = findViewById(R.id.progress_bar);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void submitPromoCode() {
        Util.hideKeyboard(mPromoCode, PromoCodeActivity.this);
        showProgress(true);
        int userIDnum = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        final String body = "userID=" + userIDnum +
                "&promoCode=" + mPromoCode.getText().toString();
        RequestQueue queue = Volley.newRequestQueue(PromoCodeActivity.this);
        String url = getResources().getString(R.string.url_app) + "/updatePromoCode.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        System.out.println("respuesta: " + response);
                        if (!response.contains("1")) {
                            Toast.makeText(PromoCodeActivity.this, response, Toast.LENGTH_LONG).show();
                        } else if (response.contains("1")) {
                            Toast.makeText(PromoCodeActivity.this, R.string.info_save, Toast.LENGTH_LONG).show();
                            onBackPressed();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(PromoCodeActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == submit.getId()) {
            if (!mPromoCode.getText().toString().isEmpty()) {
                submitPromoCode();
            }
        }
    }
}
