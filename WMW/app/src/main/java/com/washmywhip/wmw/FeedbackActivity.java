package com.washmywhip.wmw;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView feedback;
    private EditText mFeedbackInfo;
    private Button submit;
    private View mProgressView;
    private SharedPreferences mSharedPreferences;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private TextView mDescFeedback;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        feedback = (TextView) findViewById(R.id.feedback);
        mFeedbackInfo = (EditText) findViewById(R.id.feedback_info);
        mFeedbackInfo.setTypeface(mFontAvenir);
        submit = (Button) findViewById(R.id.submitFeedbackButton);
        submit.setOnClickListener(this);
        mDescFeedback = (TextView) findViewById(R.id.desc_feedback);
        mDescFeedback.setTypeface(mFontAvenir);
        feedback.setTypeface(mFont);
        submit.setTypeface(mFont);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mProgressView = findViewById(R.id.progress_bar);
        back = (ImageView) findViewById(R.id.feedback_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void submitFeedback() {
        showProgress(true);
        int userIDnum = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        final String body = "userID=" + userIDnum +
                "&message=" + mFeedbackInfo.getText().toString();
        RequestQueue queue = Volley.newRequestQueue(FeedbackActivity.this);
        String url = getResources().getString(R.string.url_app) + "/submitFeedback.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        System.out.println("respuesta: " + response);
                        if (!response.contains("1")) {
                            Toast.makeText(FeedbackActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
                        } else if (response.contains("1")) {
                            Toast.makeText(FeedbackActivity.this, R.string.info_save, Toast.LENGTH_LONG).show();
                            onBackPressed();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(FeedbackActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == submit.getId()) {
            if (!mFeedbackInfo.getText().toString().isEmpty()) {
                submitFeedback();
            }
        }
    }
}
