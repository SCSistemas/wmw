package com.washmywhip.wmw;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.KeyListener;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.washmywhip.wmw.adapters.CarAdapter;
import com.washmywhip.wmw.adapters.RecyclerItemClickListener;
import com.washmywhip.wmw.classes.Car;
import com.washmywhip.wmw.services.WashMyWhipEngine;
import com.washmywhip.wmw.util.RoundedImageView;
import com.washmywhip.wmw.util.SpacesItemDecoration;
import com.washmywhip.wmw.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    SharedPreferences mSharedPreferences;
    String first, last, email, phone;
    RoundedImageView profilePicture;
    EditText firstNameEditText;
    EditText lastNameEditText;
    EditText emailEditText;
    EditText phoneEditText;
    TextView editButton;
    RelativeLayout addCar;
    KeyListener defaultKeyListener;
    private ArrayList<Car> mCars;
    private GridLayoutManager mLayoutManager;
    private CarAdapter mCarAdapter;
    private WashMyWhipEngine mWashMyWhipEngine;
    RecyclerView mView;
    private String encodedProfile;
    TextView cars;
    TextView addCarProfile;
    TextView account;
    ImageView onBack;
    private View mProgressView;
    private Typeface mFont;
    private Typeface mFontAvenir;
    private Button mFinishedSession;
    static final int REQUEST_IMAGE_CAPTURE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mWashMyWhipEngine = new WashMyWhipEngine();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(getAssets(), "fonts/Avenir.ttc");
        profilePicture = (RoundedImageView) findViewById(R.id.pictureProfile);
        firstNameEditText = (EditText) findViewById(R.id.firstNameProfile);
        lastNameEditText = (EditText) findViewById(R.id.lastNameProfile);
        emailEditText = (EditText) findViewById(R.id.emailProfile);
        phoneEditText = (EditText) findViewById(R.id.phoneProfile);
        editButton = (TextView) findViewById(R.id.editButton);
        addCar = (RelativeLayout) findViewById(R.id.addCar);
        mView = (RecyclerView) findViewById(R.id.carGridView);
        cars = (TextView) findViewById(R.id.carsProfile);
        addCarProfile = (TextView) findViewById(R.id.addCarProfile);
        account = (TextView) findViewById(R.id.accountProfile);
        mProgressView = findViewById(R.id.progress_bar);
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mFinishedSession = (Button) findViewById(R.id.finished_session);
        mFinishedSession.setTypeface(mFont);
        mFinishedSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogout();
            }
        });
        editButton.setOnClickListener(this);
        profilePicture.setOnClickListener(null);
        editButton.setTypeface(mFontAvenir);
        addCar.setOnClickListener(this);
        account.setTypeface(mFont);
        cars.setTypeface(mFont);
        addCarProfile.setTypeface(mFont);
        account.setTypeface(mFont);
        firstNameEditText.setTypeface(mFont);
        lastNameEditText.setTypeface(mFont);
        emailEditText.setTypeface(mFont);
        phoneEditText.setTypeface(mFont);
        mLayoutManager = new GridLayoutManager(ProfileActivity.this, 1);
        mView.setLayoutManager(mLayoutManager);
        mCarAdapter = new CarAdapter(ProfileActivity.this, new ArrayList<Car>());
        mView.setAdapter(mCarAdapter);
        mView.addItemDecoration(new SpacesItemDecoration(8));
        mView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        final Car selected = mCarAdapter.getCar(position);
                        AlertDialog.Builder ADBuilder = new AlertDialog.Builder(ProfileActivity.this);
                        final ArrayAdapter arrayAdapter = new ArrayAdapter(ProfileActivity.this, android.R.layout.select_dialog_item);
                        arrayAdapter.add("Edit");
                        arrayAdapter.add("Delete");
                        ADBuilder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int _item) {
                                if (arrayAdapter.getItem(_item).toString().equalsIgnoreCase(getResources().getString(R.string.edit))) {
                                    Toast.makeText(ProfileActivity.this, arrayAdapter.getItem(_item).toString() + " " + selected.getModel(), Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(ProfileActivity.this, AddCarActivity.class);
                                    i.putExtra("car", selected);
                                    i.putExtra("position", position);
                                    startActivityForResult(i, 5);
                                }

                                if (arrayAdapter.getItem(_item).toString().equalsIgnoreCase(getResources().getString(R.string.delete))) {
                                    Toast.makeText(ProfileActivity.this, arrayAdapter.getItem(_item).toString() + " " + selected.getModel(), Toast.LENGTH_SHORT).show();
                                    deleteCar(position);
                                }
                            }
                        });

                        ADBuilder.show();//Mostramos el dialogo

                    }
                })
        );
        showProgress(true);
        int userid = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        System.out.println("pic: " + getResources().getString(R.string.url_app) + "/ClientAvatarImages/client" + userid + "avatar.jpg");
        if (userid >= 0) {
            Picasso.with(ProfileActivity.this)
                    .load(getResources().getString(R.string.url_app) + "/ClientAvatarImages/client" + userid + "avatar.jpg")
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .resize(200, 200)
                    .centerCrop().placeholder(R.drawable.picture).error(R.drawable.picture).into(profilePicture);
        }
        mWashMyWhipEngine.getUserWithID(userid, new Callback<Object>() {
            @Override
            public void success(Object object, Response response) {
                String s = object.toString();
                Log.d("getUser", s);
                String[] info = s.split(",");
                first = info[2].replace(" FirstName=", "");
                last = info[3].replace(" LastName=", "");
                email = info[4].replace(" Email=", "");
                phone = info[5].replace(" Phone=", "");
                firstNameEditText.setText(first);
                lastNameEditText.setText(last);
                emailEditText.setText(email);
                phoneEditText.setText(phone);
                defaultKeyListener = firstNameEditText.getKeyListener();
                firstNameEditText.setActivated(false);
                firstNameEditText.setKeyListener(null);
                firstNameEditText.setEnabled(false);
                lastNameEditText.setKeyListener(null);
                lastNameEditText.setEnabled(false);
                emailEditText.setKeyListener(null);
                emailEditText.setEnabled(false);
                phoneEditText.setKeyListener(null);
                phoneEditText.setEnabled(false);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("getUser", error.toString());
                Toast.makeText(ProfileActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                showProgress(false);
            }
        });
        addCarsToView();
    }


    public void deleteCar(final int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Hold on!");
        builder.setMessage("Are you sure you want to delete this car?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Car pendingDeleteCar = mCarAdapter.getCar(position);
                mCarAdapter.remove(position);
                mWashMyWhipEngine.deleteCar(pendingDeleteCar.getCarID(), new Callback<Object>() {
                    @Override
                    public void success(Object s, Response response) {
                        Log.d("Delete Car", "success: " + s.toString());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("Delete Car", "failz: " + error.toString());
                    }
                });

                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public void addCarsToView() {
        showProgress(true);
        int userIDnum = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        final ArrayList<Car> theCars = new ArrayList<Car>();
        if (userIDnum >= 0) {
            mWashMyWhipEngine.getCars(userIDnum, new Callback<List<JSONObject>>() {
                @Override
                public void success(List<JSONObject> jsonObject, Response response) {
                    String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                    JSONArray mArray = null;
                    try {
                        mArray = new JSONArray(responseString);
                        for (int i = 0; i < mArray.length(); i++) {
                            JSONObject car = mArray.getJSONObject(i);
                            Car newCar = new Car(car);
                            theCars.add(newCar);
                            Log.d("getCars", " car: " + car.toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    showProgress(false);
                    mCars = theCars;
                    if (mCars != null) {
                        if (mCars.size() > 0) {
                            for (int i = 0; i < mCars.size(); i++) {
                                mCarAdapter.add(mCars.get(i));
                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("getCars", "error: " + error.toString());
                    showProgress(false);
                }
            });
        }
    }

    public void initEditable() {
        editButton.setText("Save");
        firstNameEditText.setKeyListener(defaultKeyListener);
        firstNameEditText.setEnabled(true);
        lastNameEditText.setKeyListener(defaultKeyListener);
        lastNameEditText.setEnabled(true);
        emailEditText.setKeyListener(defaultKeyListener);
        emailEditText.setEnabled(true);
        phoneEditText.setKeyListener(defaultKeyListener);
        phoneEditText.setEnabled(true);
        profilePicture.setOnClickListener(this);
        EditText[] fields = {firstNameEditText, lastNameEditText, emailEditText, phoneEditText};

        for (EditText field : fields) {
            if (field.hasFocus()) {
                Util.hideKeyboard(field, ProfileActivity.this);
            }
        }
    }

    public void initNotEditable() {
        editButton.setText("Edit");
        firstNameEditText.setActivated(false);
        firstNameEditText.setKeyListener(null);
        firstNameEditText.setEnabled(false);
        lastNameEditText.setKeyListener(null);
        lastNameEditText.setEnabled(false);
        emailEditText.setKeyListener(null);
        emailEditText.setEnabled(false);
        phoneEditText.setKeyListener(null);
        phoneEditText.setEnabled(false);
        first = firstNameEditText.getText().toString();
        last = lastNameEditText.getText().toString();
        email = emailEditText.getText().toString();
        phone = phoneEditText.getText().toString();
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        showProgress(true);
                        int userid = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
                        mWashMyWhipEngine.updateUserInfo(userid, email, first, last, phone, new Callback<Object>() {
                            @Override
                            public void success(Object o, Response response) {
                                Log.d("updateUser", "success " + o.toString());
                                mSharedPreferences.edit().putString("FirstName", first).commit();
                                mSharedPreferences.edit().putString("LastName", last).commit();
                                mSharedPreferences.edit().putString("Email", email).commit();
                                mSharedPreferences.edit().putString("Phone", phone).commit();
                                Toast.makeText(ProfileActivity.this, R.string.saved_info_profile, Toast.LENGTH_SHORT).show();
                                showProgress(false);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Log.d("updateUser", "failz " + error.toString());
                                Toast.makeText(ProfileActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                                showProgress(false);
                            }
                        });
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setMessage(R.string.sure_save_info_profile).setPositiveButton(R.string.ok, dialogClickListener).setNegativeButton(R.string.cancel, dialogClickListener).show();
        firstNameEditText.setText(first);
        lastNameEditText.setText(last);
        emailEditText.setText(email);
        phoneEditText.setText(phone);
        firstNameEditText.setTypeface(mFont);
        lastNameEditText.setTypeface(mFont);
        emailEditText.setTypeface(mFont);
        phoneEditText.setTypeface(mFont);
        defaultKeyListener = firstNameEditText.getKeyListener();
        editButton.setText("Edit");
        firstNameEditText.setActivated(false);
        firstNameEditText.setKeyListener(null);
        firstNameEditText.setEnabled(false);
        lastNameEditText.setKeyListener(null);
        lastNameEditText.setEnabled(false);
        emailEditText.setKeyListener(null);
        emailEditText.setEnabled(false);
        phoneEditText.setKeyListener(null);
        phoneEditText.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == editButton.getId()) {
            if (editButton.getText().toString().equals("Save")) {
                initNotEditable();
            } else if (editButton.getText().toString().equals("Edit")) {
                initEditable();
            }
        } else if (v.getId() == addCar.getId()) {
            Intent i = new Intent(ProfileActivity.this, AddCarActivity.class);
            startActivityForResult(i, 6);
        } else if (v.getId() == profilePicture.getId()) {
            selectImage();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == 1 && resultCode == RESULT_OK) {
                super.onActivityResult(requestCode, resultCode, data);
                Uri photoUri = data.getData();
                Bitmap selectedImage = null;
                byte[] byteArray = null;
                try {
                    showProgress(true);
                    selectedImage = Bitmap.createBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), photoUri));
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                    byteArray = stream.toByteArray();
                    encodedProfile = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    profilePicture.setImageBitmap(selectedImage);
                    int userid = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
                    mWashMyWhipEngine.uploadClientAvatarImageAndroid(userid, encodedProfile, new Callback<Object>() {
                        @Override
                        public void success(Object s, Response response) {
                            Toast.makeText(ProfileActivity.this, R.string.photo_upload, Toast.LENGTH_SHORT).show();
                            showProgress(false);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("vendorAvatarUpload", "error: " + error.getMessage());
                            Toast.makeText(ProfileActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                            showProgress(false);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                    byte[] byteArray = null;
                    showProgress(true);
                    Bundle extras = data.getExtras();
                    Bitmap selectedImage = (Bitmap) extras.get("data");
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                    byteArray = stream.toByteArray();
                    encodedProfile = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    profilePicture.setImageBitmap(selectedImage);
                    int userid = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
                    mWashMyWhipEngine.uploadClientAvatarImageAndroid(userid, encodedProfile, new Callback<Object>() {
                        @Override
                        public void success(Object s, Response response) {
                            Log.d("vendorAvatarUpload", "Success " + s.toString());
                            Toast.makeText(ProfileActivity.this, R.string.photo_upload, Toast.LENGTH_SHORT).show();
                            showProgress(false);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("vendorAvatarUpload", "error: " + error.getMessage());
                            Toast.makeText(ProfileActivity.this, R.string.error_try_again, Toast.LENGTH_SHORT).show();
                            showProgress(false);
                        }
                    });
                } else {
                    if (requestCode == 5) {
                        if (resultCode == Activity.RESULT_OK) {
                            Car car = data.getParcelableExtra("car");
                            int pos = data.getIntExtra("position", -1);
                            mCarAdapter.remove(pos);
                            mCarAdapter.add(car);
                        }
                    } else {
                        if (requestCode == 6) {
                            if (resultCode == Activity.RESULT_OK) {
                                Car car = data.getParcelableExtra("car");
                                mCarAdapter.add(car);
                            }
                        }
                    }
                }
            }
        }
    }

    private void selectImage() {
        final CharSequence[] items = {getResources().getString(R.string.take_photo), getResources().getString(R.string.chooose_library), getResources().getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(R.string.add_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getResources().getString(R.string.take_photo))) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                } else if (items[item].equals(getResources().getString(R.string.chooose_library))) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 1);
                } else if (items[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void attemptLogout() {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.sign_out));
        builder.setMessage(getResources().getString(R.string.sure_sign_out));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mSharedPreferences.edit().clear().commit();
                mSharedPreferences.edit().putBoolean("isLoggedIn", false).commit();
                Intent i = new Intent(ProfileActivity.this, WelcomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }
}
