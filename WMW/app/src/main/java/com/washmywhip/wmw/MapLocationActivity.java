package com.washmywhip.wmw;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.washmywhip.wmw.services.WashMyWhipEngine;
import com.washmywhip.wmw.util.Util;
import com.washmywhip.wmw.util.WMW;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class MapLocationActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    private static final String TAG = "MapLocationActivity";
    private SharedPreferences mSharedPreferences;
    private Activity mActivity;
    private Typeface mFont;
    private WashMyWhipEngine mWashMyWhipEngine;
    private ProgressBar mProgressView;
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private LatLng currentLocation;
    private LatLng cameraLocation;
    private Geocoder mGeocoder;
    private Marker mMarker;
    private TextView addressText;
    private String mAddress;
    boolean isSetup;
    private ImageView onBack;
    private Button mButtonSetLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_location);
        Fabric.with(this, new Crashlytics());
        mActivity = this;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mWashMyWhipEngine = new WashMyWhipEngine();
        mGeocoder = new Geocoder(mActivity);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        View myLocationButton = mapFragment.getView().findViewById(0x2);

        if (myLocationButton != null && myLocationButton.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            // location button is inside of RelativeLayout
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) myLocationButton.getLayoutParams();

            // Align it to - parent BOTTOM|LEFT
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);

            // Update margins, set to 10dp
            final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10,
                    getResources().getDisplayMetrics());
            params.setMargins(margin, margin, margin, margin);

            myLocationButton.setLayoutParams(params);
        }
        mapFragment.getMapAsync(this);
        onBack = (ImageView) findViewById(R.id.onBack);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mButtonSetLocation = (Button) findViewById(R.id.setLocationButton);
        mButtonSetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!addressText.getText().toString().isEmpty()) {
                    mMarker = mMap.addMarker(new MarkerOptions().position(cameraLocation).visible(false));
                    Toast.makeText(MapLocationActivity.this, addressText.getText(), Toast.LENGTH_SHORT).show();
                    if (currentLocation != null) {
                        WMW.getSchedule().setLat(currentLocation.latitude);
                        WMW.getSchedule().setLng(currentLocation.longitude);
                        System.out.println("lat: " + WMW.getSchedule().getLat());
                        System.out.println("lng: " + WMW.getSchedule().getLng());
                    }
                    String address[] = addressText.getText().toString().split(",");
                    if (address[0] != null) {
                        System.out.println(address[0]);
                        WMW.getSchedule().setLocationName(address[0].toString());
                        WMW.getSchedule().setLocationStreet(address[0].toString());
                    }
                    if (address[1] != null) {
                        System.out.println(address[1]);
                        WMW.getSchedule().setLocationCity(address[1].toString());
                    }
                    if (address.length > 2) {
                        if (address[2] != null) {
                            System.out.println(address[2]);
                            WMW.getSchedule().setLocationState(address[2].toString());
                        }
                    }

                    if (address.length > 3) {
                        if (address[3] != null) {
                            System.out.println(address[3]);
                            WMW.getSchedule().setLocationCountry(address[3].toString());
                        }
                    }
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", WMW.getSchedule().getLocationName());
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            }
        });
        mButtonSetLocation.setTypeface(mFont);
        mProgressView = (ProgressBar) findViewById(R.id.progress_bar);

        addressText = (EditText) findViewById(R.id.addressText);
        addressText.setTypeface(mFont);
        addressText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.d(TAG, "entered address");
                    setCameraToUserInput();
                }
                return false;
            }
        });
    }

    public void setCameraToUserInput() {
        List<Address> addresses = null;
        try {
            addresses = mGeocoder.getFromLocationName(addressText.getText().toString(), 5);
        } catch (IOException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
        if (addresses == null) {

        } else if (addresses.size() > 0) {
            currentLocation = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            LatLng inputLocation = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(inputLocation, 16.0f));
            addressText.setText(addresses.get(0).getAddressLine(0));
        } else {
            Util.showAlertDialogDismiss(MapLocationActivity.this, "Invalid Location","Please Provide the Proper Place");
        }
    }

    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());

            if (currentLocation != null && mMap != null) {
                //Only force a camera update if user has traveled 0.1 miles from last location
                if (Util.distance(currentLocation.latitude, currentLocation.longitude, loc.latitude, loc.longitude) > 0.1) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
                    currentLocation = loc;
                }

            } else {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
            }
            currentLocation = loc;
            mMap.setOnMyLocationChangeListener(null);
        }
    };

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            Log.d("LocationListener", "got this location: " + location.toString());
            currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            if (!isSetup) {
                isSetup = true;
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 16.0f));
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public JSONObject getLocationInfo(double lat, double lng) {
        HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=true");
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            stream.close();
        } catch (ClientProtocolException e) {
            Crashlytics.logException(e);
        } catch (IOException e) {
            Crashlytics.logException(e);
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
        return jsonObject;
    }

    private GoogleMap.OnCameraChangeListener myCameraChangeListener = new GoogleMap.OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition cameraPosition) {
            cameraLocation = cameraPosition.target;
            JSONObject ret = getLocationInfo(cameraLocation.latitude, cameraLocation.longitude);
            currentLocation = new LatLng(cameraLocation.latitude, cameraLocation.longitude);
            JSONObject location;
            String location_string;
            try {
                if (ret.getJSONArray("results") != null) {
                    JSONArray results = ret.getJSONArray("results");
                    if (results.length() > 0) {
                        if (ret.getJSONArray("results").getJSONObject(0) != null) {
                            location = ret.getJSONArray("results").getJSONObject(0);
                            location_string = location.getString("formatted_address");
                            Log.d("test", "formattted address:" + location_string);
                            mAddress = location_string;
                            addressText.setText(mAddress);
                            if (addressText.hasFocus()) {
                                Util.hideKeyboard(addressText, mActivity);
                            }
                        }
                    }
                }
            } catch (JSONException e1) {
                Crashlytics.logException(e1);
                e1.printStackTrace();
            }
        }
    };

    private GoogleMap.OnMapClickListener myMapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
            Log.d(TAG, "clearing focus on address text");
            if (addressText != null && addressText.hasFocus()) {
                Log.d(TAG, "address text has focus");
                Util.hideKeyboard(addressText, mActivity);
            }
        }
    };

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.d("TEST", "map ready");
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationChangeListener(myLocationChangeListener);
        mMap.setOnCameraChangeListener(myCameraChangeListener);
        mMap.setOnMapClickListener(myMapClickListener);
    }
}
