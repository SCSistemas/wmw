package com.washmywhip.wmw.util;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.washmywhip.wmw.classes.Schedule;

/**
 * Created by enny.querales on 7/6/2016.
 */
public class WMW extends Application {

    public static final String TAG = WMW.class.getSimpleName();
    private static WMW mInstance;
    private static Schedule schedule;

    public static synchronized WMW getInstance() {
        return mInstance;
    }

    public static Schedule getSchedule() {
        return schedule;
    }

    public static void setSchedule(Schedule schedule) {
        WMW.schedule = schedule;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
}
