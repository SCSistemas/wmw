package com.washmywhip.wmw.classes;

/**
 * Created by enny.querales on 14/8/2016.
 */
public class Generic implements Comparable<Generic> {
    private int id;
    private String left;
    private String right;

    public Generic(int id, String left, String right) {
        this.setId(id);
        this.setLeft(left);
        this.setRight(right);
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String title) {

        this.left = title;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    @Override
    public int compareTo(Generic o) {
        String a = new String(String.valueOf(this.getLeft()));
        String b = new String(String.valueOf(o.getLeft()));
        return a.compareTo(b);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
