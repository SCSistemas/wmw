package com.washmywhip.wmwvendor.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.washmywhip.wmwvendor.R;
import com.washmywhip.wmwvendor.classes.Schedule;
import com.washmywhip.wmwvendor.util.Util;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by enny.querales on 12/7/2016.
 */
public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {

    private Activity mActivity;
    private List<Schedule> result;
    private static ClickListener clickListener;
    private Typeface mFont;
    private Typeface mFontAvenir;

    public ScheduleAdapter(Activity context, List<Schedule> result) {
        this.result = result;
        this.mActivity = context;
        mFont = Typeface.createFromAsset(context.getAssets(), "fonts/Archive.otf");
        mFontAvenir = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
    }

    @Override
    public ScheduleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(final ScheduleAdapter.ViewHolder holder, final int position) {
        final Schedule schedule = result.get(position);
        holder.dateSchedule.setText(schedule.getDate());
        holder.dateSchedule.setTypeface(mFont);
        String[] sepStart = schedule.getStartTime().toString().split("\\s+");
        String parsedHourStart = "";
        if (sepStart[1] != null) {
            parsedHourStart = new SimpleDateFormat("HH:mm a").format(Util.getHour(sepStart[1].toString()));
        }

        String[] sepEnd = schedule.getEndTime().toString().split("\\s+");
        String parsedHourEnd = "";
        if (sepEnd[1] != null) {
            parsedHourEnd = new SimpleDateFormat("HH:mm a").format(Util.getHour(sepEnd[1].toString()));
        }

        holder.hourSchedule.setText(parsedHourStart + " - " + parsedHourEnd);
        holder.locationSchedule.setText(schedule.getLocationName());
        holder.locationSchedule.setTypeface(mFontAvenir);


    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView hourSchedule;
        public TextView dateSchedule;
        public TextView locationSchedule;

        public ViewHolder(View mView) {
            super(mView);
            mView.setOnClickListener(this);
            hourSchedule = (TextView) mView.findViewById(R.id.hourSchedule);
            dateSchedule = (TextView) mView.findViewById(R.id.dateSchedule);
            locationSchedule = (TextView) mView.findViewById(R.id.locationSchedule);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        ScheduleAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    @Override
    public int getItemCount() {
        return result.size();
    }

    public void add(Schedule schedule) {
        result.add(schedule);
        notifyItemInserted(result.size() - 1);
    }

    public void remove(int position) {
        result.remove(position);
        notifyItemRemoved(position);
    }

    public Schedule getSchedule(int position) {
        return result.get(position);
    }
}

