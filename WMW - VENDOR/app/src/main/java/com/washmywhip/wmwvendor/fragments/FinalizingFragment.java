package com.washmywhip.wmwvendor.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.washmywhip.wmwvendor.R;
import com.washmywhip.wmwvendor.util.RoundedImageView;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class FinalizingFragment extends DialogFragment {

    private Typeface mFont;
    private Button finalizingSubmit;
    private RatingBar ratingBar;
    private SharedPreferences mSharedPreferences;
    private EditText finalizingComments;
    private RoundedImageView userImageFinalizing;

    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface FinalizingFragmentDialogListener {
        public void onDialogFinalizing(int rating, String comments);
    }

    private FinalizingFragmentDialogListener mListener;

    public static FinalizingFragment newInstance(FinalizingFragmentDialogListener mListener) {

        FinalizingFragment fragment = new FinalizingFragment();
        fragment.mListener = mListener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_init_finalizing);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Archive.otf");
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        finalizingSubmit = (Button) dialog.findViewById(R.id.finalizingSubmitButton);
        finalizingSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rating = ratingBar.getProgress();
                String comments = finalizingComments.getText().toString();
                mListener.onDialogFinalizing(rating, comments);
                dialog.dismiss();
            }
        });
        finalizingSubmit.setTypeface(mFont);
        ratingBar = (RatingBar) dialog.findViewById(R.id.finalizingRating);
        finalizingComments = (EditText) dialog.findViewById(R.id.finalizingComments);
        TextView userNameWashing = (TextView) dialog.findViewById(R.id.finalizingUserName);
        TextView howWouldYouRate = (TextView) dialog.findViewById(R.id.finalizingHowWouldYouRate);
        howWouldYouRate.setTypeface(mFont);
        userNameWashing.setTypeface(mFont);
        String name = mSharedPreferences.getString("userFullName", "Username");
        userNameWashing.setText(name);

        int userID = mSharedPreferences.getInt("userID", -1);
        userImageFinalizing = (RoundedImageView) dialog.findViewById(R.id.finalizingUserImage);
        if (userID > 0) {
            Picasso.with(getActivity())
                    .load("http://www.WashMyWhip.us/wmwapp/ClientAvatarImages/client" + userID + "avatar.jpg")
                    .resize(100, 100)
                    .centerCrop()
                    .into(userImageFinalizing);
        }

        dialog.show();
        return dialog;
    }
}


