package com.washmywhip.wmw;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.washmywhip.wmw.adapters.GenericListAdapter;
import com.washmywhip.wmw.classes.Car;
import com.washmywhip.wmw.classes.Generic;
import com.washmywhip.wmw.fragments.CommentsFragment;
import com.washmywhip.wmw.fragments.SelectCarScheduleFragment;
import com.washmywhip.wmw.services.WashMyWhipEngine;
import com.washmywhip.wmw.util.Util;
import com.washmywhip.wmw.util.WMW;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by enny.querales on 2/7/2016.
 */
public class ScheduleActivity extends Activity implements SelectCarScheduleFragment.SelectCarScheduleDialogListener, CommentsFragment.CommentsFragmentDialogListener, AdapterView.OnItemClickListener {

    private ImageView onBack;
    private TextView mTextSelectEarlier;
    private LinearLayout mLinearSelectEarlier;
    private TextView mTextEarlier;
    private TextView mTextSelectLater;
    private LinearLayout mLinearSelectLater;
    private TextView mTextLater;
    private TextView mTextSelectVehicle;
    private LinearLayout mLinearSelectVehicle;
    private TextView mTextVehicle;
    private TextView mTextSelectWashType;
    private LinearLayout mLinearSelectWashType;
    private TextView mTextWashType;
    private TextView mTextSelectLocation;
    private LinearLayout mLinearSelectLocation;
    private TextView mTextLocation;
    private TextView mTextSelectComments;
    private LinearLayout mLinearSelectComments;
    private TextView mTextComments;
    private Typeface mFont;
    private Button mButtonSchedule;
    private SharedPreferences mSharedPreferences;
    private WashMyWhipEngine mWashMyWhipEngine;
    private String costWash;
    private String costFull;
    private String costExtra;
    private View mProgressView;
    private int userID;
    ArrayList<Car> theCars = new ArrayList<Car>();
    private int hBegin = 0;
    private int hEnd = 0;

    private Date dateBegin = null;
    private Date dateEnd = null;
    private double price = 0;
    private String discount;
    private String promoCodeID;
    private String costoWashType;
    private ArrayList<Generic> mWashes = new ArrayList<Generic>();
    private ArrayList<Generic> mCars = new ArrayList<Generic>();
    ListView washList;
    ListView carList;
    private LinearLayout mListWash;
    private LinearLayout mListCar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Archive.otf");
        onBack = (ImageView) findViewById(R.id.onBack);
        mWashMyWhipEngine = new WashMyWhipEngine();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ScheduleActivity.this);
        onBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mTextSelectEarlier = (TextView) findViewById(R.id.select_earlier_than);
        mLinearSelectEarlier = (LinearLayout) findViewById(R.id.select_linear_earlier_than);
        mTextEarlier = (TextView) findViewById(R.id.earlier_than);
        mProgressView = findViewById(R.id.progress_bar);
        mLinearSelectEarlier.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(ScheduleActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if (timePicker.isShown()) {
                            // This method will return true only once
                            int hour = timePicker.getCurrentHour();
                            int min = timePicker.getCurrentMinute();
                            String state = "AM";
                            if (hour > 12) {
                                hour -= 12;
                                state = "PM";
                            }
                            Calendar calendar = GregorianCalendar.getInstance();
                            calendar.setTime(new Date());
                            calendar.get(Calendar.HOUR);
                            String stateNow = "";
                            if (calendar.get(Calendar.AM_PM) == 0)
                                stateNow = " AM";
                            else
                                stateNow = " PM";
                            if (calendar.get(Calendar.HOUR) == hour && stateNow.contains(state)) {
                                hour++;
                            }
                            Formatter fmtHour = new Formatter();
                            Formatter fmtMin = new Formatter();
                            String time = fmtHour.format("%02d", hour) + ":" + fmtMin.format("%02d", min) + " " + state;
                            if ((state.equalsIgnoreCase("AM") && hour >= 8) || (state.equalsIgnoreCase("PM") && hour <= 8)) {
                                mTextEarlier.setText(time);
                                String startTime = WMW.getSchedule().getDate() + " " + hour + ":" + min + " " + state;
                                WMW.getSchedule().setStartTime(startTime);
                                hBegin = hour;
                                System.out.println("date begin: " + dateBegin);
                                dateBegin = Util.getDate(WMW.getSchedule().getDate() + " " + hour + ":" + min);
                                System.out.println("date begin: " + dateBegin);

                            /*    if (hEnd > 0) {
                                    if (hour > hEnd) {
                                        Toast.makeText(ScheduleActivity.this, R.string.date_incorrect, Toast.LENGTH_SHORT).show();
                                        mTextEarlier.setText("");
                                        WMW.getSchedule().setStartTime("");
                                    }
                                }*/

                                if (dateEnd != null) {
                                    if (Util.isBefore(dateEnd, dateBegin)) {
                                        Toast.makeText(ScheduleActivity.this, R.string.date_incorrect, Toast.LENGTH_SHORT).show();
                                        mTextEarlier.setText("");
                                        WMW.getSchedule().setStartTime("");
                                    }
                                }
                            } else {
                                setAlertTimeError(state, hour);
                                mTextEarlier.setText("");
                                WMW.getSchedule().setStartTime("");
                            }
                        }
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        mTextSelectLater = (TextView) findViewById(R.id.select_later_than);
        mLinearSelectLater = (LinearLayout) findViewById(R.id.select_linear_later_than);
        mTextLater = (TextView) findViewById(R.id.later_than);
        mLinearSelectLater.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(ScheduleActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if (timePicker.isShown()) {
                            int hour = timePicker.getCurrentHour();
                            int min = timePicker.getCurrentMinute();
                            String state = "AM";
                            if (hour > 12) {
                                hour -= 12;
                                state = "PM";
                            }
                            Formatter fmtHour = new Formatter();
                            Formatter fmtMin = new Formatter();

                            String time = fmtHour.format("%02d", hour) + ":" + fmtMin.format("%02d", min) + " " + state;
                            if ((state.equalsIgnoreCase("AM") && hour >= 8) || (state.equalsIgnoreCase("PM") && hour <= 8)) {
                                mTextLater.setText(time);
                                String endTime = WMW.getSchedule().getDate() + " " + hour + ":" + min + " " + state;
                                WMW.getSchedule().setEndTime(endTime);
                                hEnd = hour;
                                dateEnd = Util.getDate(WMW.getSchedule().getDate() + " " + hour + ":" + min);
                                if (dateBegin != null) {
                                    if (Util.isBefore(dateEnd, dateBegin)) {
                                        Toast.makeText(ScheduleActivity.this, R.string.date_incorrect, Toast.LENGTH_SHORT).show();
                                        mTextLater.setText("");
                                        WMW.getSchedule().setEndTime("");
                                    }
                                }
                               /* if (hBegin > 0) {
                                    if (hour < hBegin) {
                                        Toast.makeText(ScheduleActivity.this, R.string.date_incorrect, Toast.LENGTH_SHORT).show();
                                        mTextLater.setText("");
                                        WMW.getSchedule().setEndTime("");
                                    }
                                }*/
                            } else {
                                setAlertTimeError(state, hour);
                                mTextLater.setText("");
                                WMW.getSchedule().setEndTime("");
                            }
                        }
                    }
                }, hour, minute, false);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        mTextSelectVehicle = (TextView) findViewById(R.id.select_vehicle);
        mLinearSelectVehicle = (LinearLayout) findViewById(R.id.select_linear_vehicle);
        mTextVehicle = (TextView) findViewById(R.id.vehicle);

        mLinearSelectVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
       /*         if (theCars.size() > 0) {
                    SelectCarScheduleFragment fragment = SelectCarScheduleFragment.newInstance(ScheduleActivity.this);
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("thecars", theCars);
                    fragment.setArguments(bundle);
                    fragment.show(getFragmentManager(), "select_car");
                }*/

                if (mListCar.getVisibility() == View.VISIBLE) {
                    mListCar.setVisibility(View.GONE);
                } else {
                    mListCar.setVisibility(View.VISIBLE);
                }
            }
        });

        mTextSelectWashType = (TextView) findViewById(R.id.select_wash_type);
        mListWash = (LinearLayout) findViewById(R.id.list_wash_type);
        mListCar = (LinearLayout) findViewById(R.id.list_car);
        mLinearSelectWashType = (LinearLayout) findViewById(R.id.select_linear_wash_type);
        mTextWashType = (TextView) findViewById(R.id.wash_type);

        mLinearSelectWashType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListWash.getVisibility() == View.VISIBLE) {
                    mListWash.setVisibility(View.GONE);
                } else {
                    mListWash.setVisibility(View.VISIBLE);
                }

            }
        });

        mTextSelectLocation = (TextView) findViewById(R.id.select_location);
        mLinearSelectLocation = (LinearLayout) findViewById(R.id.select_linear_location);
        mTextLocation = (TextView) findViewById(R.id.location);

        mLinearSelectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ScheduleActivity.this, MapLocationActivity.class);
                startActivityForResult(i, 1);
            }
        });
        mTextSelectComments = (TextView) findViewById(R.id.select_comments);
        mLinearSelectComments = (LinearLayout) findViewById(R.id.select_linear_comments);
        mTextComments = (TextView) findViewById(R.id.comments);

        mLinearSelectComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommentsFragment fragment = CommentsFragment.newInstance(ScheduleActivity.this);
                fragment.show(getFragmentManager(), "comments");
            }
        });

        mButtonSchedule = (Button) findViewById(R.id.saveSchedule);
        mButtonSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scheduleWithVolley();
            }
        });

        mTextSelectEarlier.setTypeface(mFont);
        mTextEarlier.setTypeface(mFont);
        mTextSelectLater.setTypeface(mFont);
        mTextLater.setTypeface(mFont);
        mTextSelectVehicle.setTypeface(mFont);
        mTextVehicle.setTypeface(mFont);
        mTextSelectWashType.setTypeface(mFont);
        mTextWashType.setTypeface(mFont);
        mTextSelectLocation.setTypeface(mFont);
        mTextLocation.setTypeface(mFont);
        mTextSelectComments.setTypeface(mFont);
        mTextComments.setTypeface(mFont);
        mButtonSchedule.setTypeface(mFont);

        Intent i = getIntent();
        String parent = i.getStringExtra("parent");


        washList = (ListView) findViewById(R.id.washList);
        washList.setOnItemClickListener(this);

        carList = (ListView) findViewById(R.id.carList);

        findCostWash();
        findCostFull();
        findCostExtra();

        if (parent != null) {
            mButtonSchedule.setText(getResources().getString(R.string.wash_now));
            mTextEarlier.setText(i.getStringExtra("start_time"));
            mTextLater.setText(i.getStringExtra("end_time"));
            if (i.getStringExtra("title_wash_type") != null) {
                mTextWashType.setText(i.getStringExtra("title_wash_type"));
                mTextLocation.setText(WMW.getSchedule().getLocationName());
                costoWashType = i.getStringExtra("costo_wash_type");
                System.out.println("washtype: " + i.getStringExtra("title_wash_type"));
                System.out.println("costowashtype: " + i.getStringExtra("costo_wash_type"));
                mTextWashType.setText(i.getStringExtra("title_wash_type"));
            }
        }
        getCars();
        getPromoCode();
    }

    public void setAlertTimeError(String state, int hour) {
        String title = "";
        String desc = "";
        if (state.equalsIgnoreCase("PM") && hour > 8) {
            title = getResources().getString(R.string.too_late);
            desc = getResources().getString(R.string.valid_time_schedule);
        } else {
            title = getResources().getString(R.string.too_early);
            desc = getResources().getString(R.string.valid_time_schedule_morning);
        }
        Util.showAlertDialogDismiss(ScheduleActivity.this, title, desc);
    }

    public void getCars() {
        showProgress(true);
        int userIDnum = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        theCars.clear();
        if (userIDnum >= 0) {
            mWashMyWhipEngine.getCars(userIDnum, new Callback<List<JSONObject>>() {
                @Override
                public void success(List<JSONObject> jsonObject, Response response) {
                    String responseString = new String(((TypedByteArray) response.getBody()).getBytes());
                    Log.d("getCars", " response: " + responseString);
                    if (responseString.equals("[]")) {
                        Toast.makeText(ScheduleActivity.this, R.string.schedule_add_car, Toast.LENGTH_SHORT).show();
                    } else {
                        JSONArray mArray = null;
                        try {
                            mArray = new JSONArray(responseString);
                            for (int i = 0; i < mArray.length(); i++) {
                                JSONObject car = mArray.getJSONObject(i);
                                Car newCar = new Car(car);
                                theCars.add(newCar);
                                Generic c = new Generic(newCar.getCarID(), newCar.getMake() + " " + newCar.getModel(), newCar.getPlate());
                                mCars.add(c);
                            }

                            carList.setAdapter(new GenericListAdapter(ScheduleActivity.this, R.layout.generic_item, mCars));
                            carList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                                    Generic car = mCars.get(pos);
                                    mTextVehicle.setText(car.getLeft());
                                    WMW.getSchedule().setCarID(car.getId());
                                }
                            });

                        } catch (JSONException e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                        showProgress(false);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showProgress(false);
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                mTextLocation.setText(result);
            }
        }
    }

    public void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }


    public void getPromoCode() {
        showProgress(true);
        userID = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        if (userID >= 0) {
            showProgress(true);
            mWashMyWhipEngine.getPromotionForUser(userID, new Callback<Object>() {
                @Override
                public void success(Object object, Response response) {
                    showProgress(false);
                    mButtonSchedule.setVisibility(View.VISIBLE);
                    String s = object.toString();
                    Log.d("getPromos", s);
                    String[] info = s.split(",");
                    promoCodeID = info[2].replace(" Code=", "");
                    String expired = info[3].replace(" Expired=", "");
                    Log.d("expired", expired);
                    if (expired.equalsIgnoreCase("0")) {
                        discount = info[4].replace(" Discount=", "");
                        Log.d("discount", discount);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("getPromosCode", "failz: " + error.toString());
                    mButtonSchedule.setVisibility(View.VISIBLE);
                    showProgress(false);
                }
            });
        }
    }

    public void submitPromoCode() {
        showProgress(true);
        int userIDnum = Integer.parseInt(mSharedPreferences.getString("UserID", "-1"));
        final String body = "userID=" + userIDnum +
                "&promoCode=" + promoCodeID;
        RequestQueue queue = Volley.newRequestQueue(ScheduleActivity.this);
        String url = getResources().getString(R.string.url_app) + "/updatePromoCode.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showProgress(false);
                        System.out.println("respuesta: " + response);
/*                        if (!response.contains("1")) {
                            Toast.makeText(ScheduleActivity.this, response, Toast.LENGTH_LONG).show();
                        } else if (response.contains("1")) {
                            Toast.makeText(ScheduleActivity.this, R.string.info_save, Toast.LENGTH_LONG).show();
                        }*/
                        finish();
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ScheduleActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }


    public void scheduleWithVolley() {
        if (validateSave()) {
            showProgress(true);
            String date = WMW.getSchedule().getDate();
            String startTime = "";
            String endTime = "";
            double locationLat = 0;
            double locationLon = 0;
            String locationName = "";
            String locationStreet = "";
            String locationCity = "";
            String locationState = "";
            String locationCountry = "";
            String locationZIP = "";
            int washType = -1;
            int carID = -1;
            String Comments = "";
            if (WMW.getSchedule().getStartTime() != null) {
                startTime = WMW.getSchedule().getStartTime();
            }
            if (WMW.getSchedule().getEndTime() != null) {
                endTime = WMW.getSchedule().getEndTime();
            }

            if (WMW.getSchedule().getLocationName() != null) {
                locationName = WMW.getSchedule().getLocationName();
            }

            if (WMW.getSchedule().getLocationStreet() != null) {
                locationStreet = WMW.getSchedule().getLocationStreet();
            }

            if (WMW.getSchedule().getLocationCity() != null) {
                locationCity = WMW.getSchedule().getLocationCity();
            }

            if (WMW.getSchedule().getLocationZip() != null) {
                locationZIP = WMW.getSchedule().getLocationZip();
            }

            if (WMW.getSchedule().getLocationState() != null) {
                locationState = WMW.getSchedule().getLocationState();
            }

            if (WMW.getSchedule().getLocationCountry() != null) {
                locationCountry = WMW.getSchedule().getLocationCountry();
            }

            if (WMW.getSchedule().getCarID() > 0) {
                carID = WMW.getSchedule().getCarID();
            }

            washType = WMW.getSchedule().getWashType();

            if (WMW.getSchedule().getLat() > 0) {
                locationLat = WMW.getSchedule().getLat();
            }

            if (WMW.getSchedule().getLng() < 0) {
                locationLon = WMW.getSchedule().getLng();
            }

            if (WMW.getSchedule().getComments() != null) {
                Comments = WMW.getSchedule().getComments();
            }

            if (discount != null) {
                if (discount.contains("$")) {
                    String value = discount.substring(1, discount.length());
                    price = Integer.parseInt(costoWashType) - Integer.parseInt(value);
                    Log.d("discount", value);
                    Log.d("promoCodeID", promoCodeID);
                    System.out.println("price " + price);
                }
                if (discount.contains("%")) {
                    String value = discount.substring(1, discount.length());
                    double porc = (Double.parseDouble(value) / 100) * (Double.parseDouble(costoWashType));
                    price = Double.parseDouble(costoWashType) - porc;
                    Log.d("discount", discount);
                    Log.d("promoCodeID", promoCodeID);
                    System.out.println("price " + price);
                }
            } else {
                price = Integer.parseInt(costoWashType);
            }

            String userID = mSharedPreferences.getString("UserID", null);
            final String body = "userID=" + userID +
                    "&date=" + date + "&startTime=" + startTime + "&endTime=" + endTime +
                    "&locationLat=" + locationLat + "&locationLon=" + locationLon +
                    "&locationName=" + locationName + "&locationStreet=" + locationStreet +
                    "&locationCity=" + locationCity + "&locationState=" + locationState +
                    "&locationCountry=" + locationCountry + "&locationZIP=" + locationZIP +
                    "&washType=" + washType + "&price=" + price + "&carID=" + carID + "&Comments=" + Comments;
            System.out.println("body: " + body);
            RequestQueue queue = Volley.newRequestQueue(ScheduleActivity.this);
            String url = "http://www.washmywhip.esy.es/scheduleWash.php";
            StringRequest sr = new StringRequest(Request.Method.POST, url,
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            showProgress(false);
                            if (response.contains("1")) {
                                Toast.makeText(ScheduleActivity.this, "Wash Scheduled", Toast.LENGTH_SHORT).show();
                                if (discount != null) {
                                    submitPromoCode();
                                } else {
                                    finish();
                                }
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    showProgress(false);
                    Toast.makeText(ScheduleActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
                }
            }) {

                @Override
                public byte[] getBody() throws AuthFailureError {
                    // TODO Auto-generated method stub
                    return body.getBytes();
                }

                @Override
                public String getBodyContentType() {
                    // TODO Auto-generated method stub
                    return "application/x-www-form-urlencoded";
                }

                @Override
                protected String getParamsEncoding() {
                    return "utf-8";
                }

            };
            queue.add(sr);
        } else {
            Toast.makeText(ScheduleActivity.this, R.string.fields_incomplete, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onSelectCarSchedule(Car car) {
        if (car != null) {
            mTextVehicle.setText(car.getMake() + " " + car.getModel());
            WMW.getSchedule().setCarID(car.getCarID());
        }
    }

    @Override
    public void onDialogComments(String comments) {
        if (comments != null) {
            mTextComments.setText(comments);
            WMW.getSchedule().setComments(comments);
        }
    }

    public void findCostWash() {
        final String body = "typeVal=0";
        RequestQueue queue = Volley.newRequestQueue(ScheduleActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (!response.contains(getResources().getString(R.string.error))) {
                            costWash = "$" + response;
                            Generic genericOne = new Generic(0, getResources().getString(R.string.type_wash_one), "$" + response);
                            mWashes.add(genericOne);
                            Collections.sort(mWashes);
                            washList.setAdapter(new GenericListAdapter(ScheduleActivity.this, R.layout.generic_item, mWashes));

                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ScheduleActivity.this, R.string.error_try_again, Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    public void findCostFull() {
        final String body = "typeVal=1";
        RequestQueue queue = Volley.newRequestQueue(ScheduleActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (!response.contains(getResources().getString(R.string.error))) {
                            costFull = "$" + response;
                            Generic genericTwo = new Generic(1, getResources().getString(R.string.type_wash_two), "$" + response);
                            mWashes.add(genericTwo);
                            Collections.sort(mWashes);
                            washList.setAdapter(new GenericListAdapter(ScheduleActivity.this, R.layout.generic_item, mWashes));
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);

    }

    public boolean validateSave() {
        boolean sw = false;
        if (!mTextEarlier.getText().toString().isEmpty() && !mTextLater.getText().toString().isEmpty() && !mTextVehicle.getText().toString().isEmpty()
                && !mTextWashType.getText().toString().isEmpty() && !mTextLocation.getText().toString().isEmpty()) {
            sw = true;
        }
        return sw;
    }

    public void findCostExtra() {
        final String body = "typeVal=2";
        RequestQueue queue = Volley.newRequestQueue(ScheduleActivity.this);
        String url = "http://www.washmywhip.esy.es/findCostOfTransactionType.php";
        StringRequest sr = new StringRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (!response.contains(getResources().getString(R.string.error))) {
                            costExtra = "$" + response;
                            Generic genericThree = new Generic(2, getResources().getString(R.string.type_wash_three), "$" + response);
                            mWashes.add(genericThree);
                            Collections.sort(mWashes);
                            washList.setAdapter(new GenericListAdapter(ScheduleActivity.this, R.layout.generic_item, mWashes));
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                // TODO Auto-generated method stub
                return body.getBytes();
            }

            @Override
            public String getBodyContentType() {
                // TODO Auto-generated method stub
                return "application/x-www-form-urlencoded";
            }

            @Override
            protected String getParamsEncoding() {
                return "utf-8";
            }

        };
        queue.add(sr);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Generic wash = mWashes.get(position);
        mTextWashType.setText(wash.getLeft());
        WMW.getSchedule().setWashType(wash.getId());
        System.out.println("id wash: " + wash.getId());
        if (wash.getRight() != null) {
            costoWashType = wash.getRight().substring(1, wash.getRight().length());
            System.out.println("costo: " + costoWashType);
        }
    }
}
