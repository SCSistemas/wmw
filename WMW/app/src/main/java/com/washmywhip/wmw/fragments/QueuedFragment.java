package com.washmywhip.wmw.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.washmywhip.wmw.R;
import com.washmywhip.wmw.services.ConnectionManager;

/**
 * Created by enny.querales on 5/7/2016.
 */
public class QueuedFragment extends DialogFragment {


    ConnectionManager mConnectionManager;
    /**
     * Use to return List with the ids of activitis selcted of selected Values to the caller
     * can be modified to send only  if only id is needed
     */
    public interface QueuedFragmentDialogListener {
        public void onDialogQueued(String comments);
    }

    private QueuedFragmentDialogListener mListener;

    public static QueuedFragment newInstance(QueuedFragmentDialogListener mListener) {

        QueuedFragment fragment = new QueuedFragment();
        fragment.mListener = mListener;
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_init_queued);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        Button cancelQueueButton = (Button) dialog.findViewById(R.id.cancelQueue);
        cancelQueueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDialogQueued("");
                dialog.dismiss();
            }
        });

        dialog.show();
        return dialog;
    }
}


